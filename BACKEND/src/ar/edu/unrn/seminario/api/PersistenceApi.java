package ar.edu.unrn.seminario.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import ar.edu.unrn.seminario.accesos.ConfiguracionDAOJDBC;
import ar.edu.unrn.seminario.accesos.DispositivoDAOJDBC;
import ar.edu.unrn.seminario.accesos.ParametroDAOJDBC;
import ar.edu.unrn.seminario.accesos.UnidadMedidaDAOJDBC;
import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UnidadMedidaDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.ObjectAlreadyExistException;
import ar.edu.unrn.seminario.exception.ReferenceDataException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;
import ar.edu.unrn.seminario.modelo.Configuracion;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.Parametro;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.UnidadMedida;
import ar.edu.unrn.seminario.modelo.Usuario;

public class PersistenceApi implements IApi{

	UnidadMedidaDAO unidadDAO = new UnidadMedidaDAOJDBC();
	ParametroDAO parametroDAO = new ParametroDAOJDBC();
	ConfiguracionDAO configuracionDAO= new ConfiguracionDAOJDBC();
	DispositivoDAO dispositivoDAO = new DispositivoDAOJDBC();

	@Override
	public void registrarUsuario(String username, String password, String email, String nombre, Integer rol) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UsuarioDTO obtenerUsuario(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminarUsuario(String username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<RolDTO> obtenerRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RolDTO> obtenerRolesActivos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarRol(Integer codigo, String descripcion, boolean estado) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public RolDTO obtenerRolPorCodigo(Integer codigo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void activarRol(Integer codigo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void desactivarRol(Integer codigo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<UsuarioDTO> obtenerUsuarios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void activarUsuario(String username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void desactivarUsuario(String username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Configuracion buscarConfiguracion(String nombre) throws ConnectionFailureException 
	{
		Configuracion temp = configuracionDAO.find(nombre);
		
		return temp;
	}

	@Override
	public List<ConfiguracionDTO> filtrarConfiguracionDTO(String nombre, List<ConfiguracionDTO> lista) throws ConnectionFailureException 
	{
		Set <Configuracion> listaC= configuracionDAO.findAll();
		List <ConfiguracionDTO>listaDTO = new ArrayList<>();
		for (Configuracion c : listaC) {
			ConfiguracionDTO dto= new ConfiguracionDTO(c);
			listaDTO.add(dto);
		}
		return (List<ConfiguracionDTO>) listaDTO.stream()
				.filter((u)->u.getNombre().toLowerCase().contains(nombre.toLowerCase()))
				.sorted((u1, u2)->
				{
					return u1.getNombre().compareToIgnoreCase(u2.getNombre());
				})
				.collect(Collectors.toList());
	}

	@Override
	public ConfiguracionDTO buscarConfiguracionDTO(String nombre) throws ConnectionFailureException {

		Configuracion temp = configuracionDAO.find(nombre);
		ConfiguracionDTO config = new ConfiguracionDTO(temp);
		return config;
	}

	@Override
	public void altaConfiguracion(String nombre, String descripcion, List<String> listaParametros)
			throws DataEmptyException, RepeatedNameException, ConnectionFailureException {
		
	        Set <Parametro> listaP = obtenerParametros(listaParametros);
	        Set <Configuracion> listaConfiguracion = configuracionDAO.findAll();
	        Configuracion nuevaConfig= new Configuracion (nombre, descripcion, listaP);
	        if (listaConfiguracion.contains(nuevaConfig)){
	        	throw new RepeatedNameException("Ya existe una configuracion con ese nombre");
	        }  
	        else
	        {
	        	configuracionDAO.create(nuevaConfig);
	        }
		}

	@Override
	public void modificarConfiguracion(String nombre, String descripcionNueva,
			List<String> listaParametros) throws DataEmptyException, ConnectionFailureException {
			
			Set <Configuracion> listaConfiguracion= configuracionDAO.findAll();
		    Iterator<Configuracion> iterator = listaConfiguracion.iterator();
		    
		    while (iterator.hasNext() ) {
			    Configuracion config = iterator.next();       
		        if (config.getNombre().equalsIgnoreCase(nombre)) {
		        	Set<Parametro> lista = new HashSet<>();
				    lista=obtenerParametros(listaParametros);
				    Configuracion reemplazo = new Configuracion(nombre, descripcionNueva, lista);
				    configuracionDAO.update(reemplazo);
		        }
		    }
		    
	}
	@Override
	public void agregarParametroaConfiguracion(String nombreConfiguracion, String nombreParametro)
			throws RepeatedNameException, ConnectionFailureException 
			{
				Configuracion config = configuracionDAO.find(nombreConfiguracion);
				if (config != null) {
					Parametro nuevo = parametroDAO.find(nombreParametro);
					config.agregarParametro(nuevo);
					configuracionDAO.update(config);

				}
					
			}
	@Override
	public 	void quitarParametroaConfiguracion(String configuracion, String parametro) throws ConnectionFailureException
		{
			Configuracion config = configuracionDAO.find(configuracion);
			config.eliminarParametro(parametro);
			configuracionDAO.update(config);
		}

	@Override
	public List<ConfiguracionDTO> getConfiguracionDTO() throws ConnectionFailureException{
		Set <Configuracion> listaTemp = configuracionDAO.findAll();
		
		List <ConfiguracionDTO> listaDTO = new ArrayList<>();
		for (Configuracion p : listaTemp) {
			ConfiguracionDTO dto = new ConfiguracionDTO(p);
			listaDTO.add(dto);
		}
		return listaDTO;
	}

	@Override
	public void eliminarConfiguracion(String nombre) throws ReferenceDataException, ConnectionFailureException 
	{
		Set <Dispositivo> listaDispositivo = dispositivoDAO.findAll();
		Configuracion config = configuracionDAO.find(nombre);
		for (Dispositivo dispositivo : listaDispositivo) {
			if (dispositivo.getConfiguracion().equals(config)) {
				throw new ReferenceDataException("No se puede eliminar. La configuracion es usada por un dispositivo");
			}
		}
	  configuracionDAO.remove(nombre);
	}
	

	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//PARAMETRO
	
	@Override
	public void altaParametro(String nombre, List<String> unidades, String descripcion, String unidadPrincipal) throws DataEmptyException, RepeatedNameException, ConnectionFailureException
	{
			Set<UnidadMedida> unidad = obtenerUnidadMedida(unidades);
			UnidadMedida principal = unidadDAO.find(unidadPrincipal);
			Parametro parametro = new Parametro(nombre, unidad, descripcion, principal);
			Set <Parametro> listaParametros = parametroDAO.findAll();
			
			if(listaParametros.contains(parametro)){
				throw new RepeatedNameException("Ya existe una unidad de medida con ese nombre");
			}
			else {
				parametroDAO.create(parametro);

			}
		}
	@Override
	public void modificarParametro(String nombre, List<String> unidades, String descripcion, String unidadPrincipal)
			throws DataEmptyException, ConnectionFailureException
	{
	
	Set <UnidadMedida>listaUnidades = obtenerUnidadMedida(unidades);
	
	UnidadMedida principal = unidadDAO.find(unidadPrincipal);
		
	    Parametro parametroModificado = new Parametro(nombre, listaUnidades, descripcion, principal);
	    parametroDAO.update(parametroModificado);
	}

	@Override
	public void eliminarParametro(String nombre) throws ReferenceDataException, ConnectionFailureException
	{
		Set<Configuracion> listaConfiguracion = configuracionDAO.findAll();
		Parametro parametro = parametroDAO.find(nombre);
	
		for (Configuracion configuracion : listaConfiguracion)
		{	
			if(configuracion.getListaParametros().contains(parametro))
			{
				throw new ReferenceDataException("Para eliminar este parametro primero debe quitar sus referencias");
			}
		}
		
		parametroDAO.remove(nombre);
	}

	@Override
	public Parametro buscarParametros(String nombre) throws ConnectionFailureException {
			Parametro param=new Parametro();
			Set <Parametro> listaParametros= parametroDAO.findAll();
			for (Parametro p : listaParametros) {
				if (p.getNombre().equalsIgnoreCase(nombre)) {
					param = p;	
				}
			}
			return param;
		}

	@Override
	public ParametroDTO buscarParametroDTO(String nombre) throws ConnectionFailureException {
			ParametroDTO param = new ParametroDTO();	
			Set <Parametro> listsBase = parametroDAO.findAll();
			for (Parametro p : listsBase) {
				if (p.getNombre().equalsIgnoreCase(nombre)) {
					param = new ParametroDTO(p);	
					
				}
			}
			return param;
		}
	@Override
	public List<ParametroDTO> filtrarParametroDTO(String query, List<ParametroDTO> lista) throws ConnectionFailureException {
		
			Set <Parametro> listaParametro= parametroDAO.findAll();
			List <ParametroDTO> listaPramaetroDTO = new ArrayList<>();

			for (Parametro p : listaParametro) {
				ParametroDTO un= new ParametroDTO(p);
				listaPramaetroDTO.add(un);
			}
			return listaPramaetroDTO.stream()
					.filter((parametro) -> parametro.getNombre().toLowerCase().contains(query.toLowerCase()) || parametro.getUnidadPrincipal().equalsIgnoreCase(query))
					.sorted((parametro1, parametro2) -> parametro1.getNombre().compareToIgnoreCase(parametro2.getNombre()))
					.collect(Collectors.toList());
		}

	@Override
	public List<ParametroDTO> getParametroDTO() throws ConnectionFailureException {
		Set <Parametro> listaTemp = parametroDAO.findAll();
		List <ParametroDTO> listaDTO = new ArrayList<>();
		for (Parametro p : listaTemp) {
			ParametroDTO dto = new ParametroDTO(p);
			listaDTO.add(dto);
		}
		return listaDTO;
	}

	@Override
	public Set<Parametro> obtenerParametros(List<String> lista) throws ConnectionFailureException {
		
		Set <Parametro> listaRetorno = new HashSet<>(); 
		for (String s : lista)
		{	
			listaRetorno.add(parametroDAO.find(s));
		}
		return listaRetorno;
	}
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//UNIDAD DE MEDIDA

	@Override
	public List<String> obtenerSimbolosUnidadMedida(List<String> nombre)throws ConnectionFailureException
	{
		Set<UnidadMedida> listaUnidadMedida = unidadDAO.findAll();
		List<String> listaSimbolos = new ArrayList<>();
		for (String texto : nombre)
		{
			for (UnidadMedida unidades : listaUnidadMedida)
				{
					if(unidades.getNombre().equals(texto))
					{
						listaSimbolos.add(unidades.getSimbolo());
					}
				}
		}
		return listaSimbolos;
	}

	@Override
	public List<String> obtenerTiposUnidadMedida(List<String> nombre)throws ConnectionFailureException
	{
		Set<UnidadMedida> listaUnidadMedida = unidadDAO.findAll();
		List<String> listaTipos = new ArrayList<>();
		for (String texto : nombre)
		{
			for (UnidadMedida unidades : listaUnidadMedida)
				{
					if(unidades.getNombre().equals(texto))
					{
						listaTipos.add(unidades.getTipoDato());
					}
				}
		}
		return listaTipos;
	}

	@Override
	public void altaUnidadMedida(String nombre, String tipoDato, String simbolo)
			throws DataEmptyException, RepeatedNameException, ConnectionFailureException {
		
		UnidadMedida unidadMedida = new UnidadMedida(nombre, tipoDato, simbolo);

		Set <UnidadMedida> listaUnidadMedida= unidadDAO.findAll();
		if(listaUnidadMedida.contains(unidadMedida)){
			throw new RepeatedNameException("Ya existe una unidad de medida con ese nombre");
		}
		else {
		     unidadDAO.create(unidadMedida);
		}
		
	}

	@Override
	public Set<UnidadMedida> obtenerUnidadMedida(List <String> lista)throws ConnectionFailureException{
		
			Set <UnidadMedida> listaP = new HashSet<>();
			Set <UnidadMedida> listaBase= unidadDAO.findAll();
			for (String texto : lista)
			{
				for (UnidadMedida unidad : listaBase)
				{
					if (unidad.getNombre().equalsIgnoreCase(texto))
					{
						listaP.add(unidad);
					}
				}
			}
			return listaP;
		}


	@Override
	public List <UnidadMedidaDTO> getUnidadMedidaDTO() throws ConnectionFailureException{
			Set <UnidadMedida> listaPura=unidadDAO.findAll();
			listaPura=unidadDAO.findAll();
			List <UnidadMedidaDTO> listaUnidadMedidaDTO= new ArrayList<>();
			for (UnidadMedida u : listaPura) {
				UnidadMedidaDTO unidadDTO = new UnidadMedidaDTO(u);
				listaUnidadMedidaDTO.add(unidadDTO);
			}
			return listaUnidadMedidaDTO;
		
	}

	@Override
	public void eliminarUnidadeMedida(String unidadSeleccionada) throws ReferenceDataException, ConnectionFailureException{
		Set<Parametro> listaParametros = parametroDAO.findAll();
		UnidadMedida unidad = unidadDAO.find(unidadSeleccionada);
	
		for (Parametro parametro : listaParametros)
		{	
			if(parametro.getUnidadMedida().contains(unidad))
			{
				throw new ReferenceDataException("Para eliminar esta unidad primero debe quitar sus referencias");
			}
		}
		
		unidadDAO.remove(unidadSeleccionada);
	}

	@Override
	public void modificarUnidadMedida(String nombre, String nuevoTipoDato, String nuevoSimbolo)throws DataEmptyException, ConnectionFailureException
	{
	    UnidadMedida unidadModificada = new UnidadMedida(nombre, nuevoTipoDato, nuevoSimbolo);
	    unidadDAO.update(unidadModificada);
	}
	
	
	@Override
	public UnidadMedida buscarUnidadMedida(String nombre) throws ConnectionFailureException
	{
		UnidadMedida unidad = unidadDAO.find(nombre);
		return unidad;
	}
	
	@Override
	public UnidadMedidaDTO buscarUnidadMedidaDTO(String nombre) throws ConnectionFailureException
	{
		UnidadMedidaDTO unidadBuscada = new UnidadMedidaDTO(buscarUnidadMedida(nombre));
		return unidadBuscada;
	}

	@Override
	public List<String> listarTiposDato(){
		List<String> listaTiposDato = new ArrayList<>();
		listaTiposDato.add("Short");
		listaTiposDato.add("Integer");
		listaTiposDato.add("Long");
		listaTiposDato.add("Float");
		listaTiposDato.add("Double");
		listaTiposDato.add("Character");
	
		return listaTiposDato;
	}

	@Override
	public List<UnidadMedidaDTO> filtrarUnidadMedida(String query, List<UnidadMedidaDTO> unidades) {	
		return unidades.stream()
			.filter((unidad) -> unidad.getNombre().toLowerCase().contains(query.toLowerCase()) || unidad.getTipoDato().equalsIgnoreCase(query) || unidad.getSimbolo().equalsIgnoreCase(query))
			.sorted((unidad1, unidad2) -> unidad1.getNombre().compareToIgnoreCase(unidad2.getNombre()))
			.collect(Collectors.toList());
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//UNIDAD DE MEDIDA
	
	@Override
	public Dispositivo devolverDispositivo(String nombre) throws ConnectionFailureException {
		Dispositivo dispositivo = dispositivoDAO.find(nombre);
		return dispositivo;
	}

	@Override
	public void desactivarDispositivo(String nombreDispositivoDTO) throws ConnectionFailureException {
		Dispositivo dispositivo = dispositivoDAO.find(nombreDispositivoDTO);
		dispositivo.desactivar();
		dispositivoDAO.update(dispositivo);
	}

	@Override
	public List<DispositivoDTO> getDispositivosDTO() throws ConnectionFailureException {
		List <DispositivoDTO> listaDTO = new ArrayList<>();
		for (Dispositivo dispositivo : dispositivoDAO.findAll()) {
			DispositivoDTO dispositivoDTO = new DispositivoDTO(dispositivo);
			listaDTO.add(dispositivoDTO);
		}
		return listaDTO;
	}

	@Override
	public void activarDispositivo(String nombreDispositivoDTO) throws ConnectionFailureException {
		Dispositivo dispositivo = dispositivoDAO.find(nombreDispositivoDTO);
		dispositivo.activar();
		dispositivoDAO.update(dispositivo);
	}

	@Override
	public DispositivoDTO recuperarDispositivoDTO(String nombreDispositivo) throws ConnectionFailureException {
		Dispositivo dispositivo = dispositivoDAO.find(nombreDispositivo);
		DispositivoDTO dispositivoDTO = new DispositivoDTO(dispositivo);
		return dispositivoDTO;
	}

	@Override
	public void altaDispositivo(String nombre, String descripcion, String modelo, String marca, boolean estado,String nombreConfig)
			throws ObjectAlreadyExistException, DataEmptyException, ConnectionFailureException, RepeatedNameException {

		Configuracion config= configuracionDAO.find(nombreConfig);
		
		Dispositivo dispositivo = new Dispositivo(nombre, descripcion, modelo, marca, estado,config);
		
		Set<Dispositivo> listaDispositivo = dispositivoDAO.findAll();
		if(listaDispositivo.contains(dispositivo))
		{
			throw new RepeatedNameException("Ya existe un dispositivo con ese nombre");
		}
		else
		{	
			if(config.getNombre() == null)
			{
				config.setNombre("");
				dispositivoDAO.create(dispositivo);
			}
			else
			{
				dispositivoDAO.create(dispositivo);
			}
		}
	}

	@Override
	public List<DispositivoDTO> ordenarPorNombreAscendente(List<DispositivoDTO> lista) throws ConnectionFailureException {
		List <DispositivoDTO> listaDTO = new ArrayList<>();
		for (Dispositivo dispositivo : dispositivoDAO.findAll()) {
			DispositivoDTO dispositivoDTO = new DispositivoDTO(dispositivo);
			listaDTO.add(dispositivoDTO);
		}
		return listaDTO;
	}

	@Override
	public void eliminarDispositivo(String nombreDispositivo) throws ConnectionFailureException {
		dispositivoDAO.remove(nombreDispositivo);
	}

	@Override
	public void modificarDispositivo(String nombreDispositivo, String nuevaDescripcion, String nuevoModelo,
		String nuevaMarca, boolean nuevoEstado, String nombreConfiguracion) throws DataEmptyException, ConnectionFailureException {
		Configuracion config = configuracionDAO.find(nombreConfiguracion);
		Dispositivo dispositivoModificado = new Dispositivo(nombreDispositivo, nuevaDescripcion, nuevoModelo,nuevaMarca,nuevoEstado,config);
		dispositivoDAO.update(dispositivoModificado);
    }


	@Override
	public void existeDispositivo(String nombreDispositivo) throws ConnectionFailureException {
		
		Dispositivo dispositivo = dispositivoDAO.find(nombreDispositivo);
		if (dispositivo!=null) {
			throw new ObjectAlreadyExistException();
			}
		}
	
	@Override
	public List<DispositivoDTO> filtrarDispositivos(String query, List<DispositivoDTO> dispositivos) {	
		return dispositivos.stream()
			.filter((dispositivo) -> dispositivo.getNombre().toLowerCase().contains(query.toLowerCase()) || dispositivo.getMarca().toLowerCase().contains(query.toLowerCase()) || dispositivo.getModelo().toLowerCase().contains(query.toLowerCase()))
			.sorted((dispositivo1, dispositivo2) -> dispositivo1.getNombre().compareToIgnoreCase(dispositivo2.getNombre()))
			.collect(Collectors.toList());
	}
}