package ar.edu.unrn.seminario.api;

import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.modelo.Dispositivo;

public interface DispositivoDAO
{

	void create(Dispositivo config) throws ConnectionFailureException;
	void update(Dispositivo config) throws ConnectionFailureException;
	Dispositivo find(String nombre) throws ConnectionFailureException;
	void remove(String nombre) throws ConnectionFailureException;
	Set<Dispositivo> findAll() throws ConnectionFailureException;

}