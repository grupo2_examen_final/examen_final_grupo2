package ar.edu.unrn.seminario.api;
import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.modelo.Configuracion;

public interface ConfiguracionDAO {

	void create(Configuracion config) throws ConnectionFailureException;
	void update(Configuracion config) throws ConnectionFailureException;
	Configuracion find(String nombre) throws ConnectionFailureException;
	void remove(String nombre) throws ConnectionFailureException;
	Set<Configuracion> findAll() throws ConnectionFailureException;

}