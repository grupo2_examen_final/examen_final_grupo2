package ar.edu.unrn.seminario.api;


import java.sql.Connection;
import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.modelo.UnidadMedida;

public interface UnidadMedidaDAO {

	
	void create(UnidadMedida unidad) throws ConnectionFailureException;
	void update(UnidadMedida unidad) throws ConnectionFailureException;
	void remove(String nombre) throws ConnectionFailureException;
	UnidadMedida find(String nombre) throws ConnectionFailureException;
	Set<UnidadMedida> findAll() throws ConnectionFailureException;

}