package ar.edu.unrn.seminario.api;
import java.sql.Connection;
import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.ReferenceDataException;
import ar.edu.unrn.seminario.modelo.Parametro;

public interface ParametroDAO {

	
	void create(Parametro parametro) throws ConnectionFailureException;
	void update(Parametro parametro) throws ConnectionFailureException;
	void remove(String nombre) throws ReferenceDataException, ConnectionFailureException;
	Parametro find(String nombre) throws ConnectionFailureException;
	Set<Parametro> findAll() throws ConnectionFailureException;
}