package ar.edu.unrn.seminario.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UnidadMedidaDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.exception.*;
import ar.edu.unrn.seminario.modelo.Configuracion;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.Parametro;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.UnidadMedida;
import ar.edu.unrn.seminario.modelo.Usuario;

public interface IApi {

	void registrarUsuario(String username, String password, String email, String nombre, Integer rol);

	UsuarioDTO obtenerUsuario(String username);

	void eliminarUsuario(String username);

	List<RolDTO> obtenerRoles();

	List<RolDTO> obtenerRolesActivos();

	void guardarRol(Integer codigo, String descripcion, boolean estado); // crear el objeto de dominio �Rol�

	RolDTO obtenerRolPorCodigo(Integer codigo); // recuperar el rol almacenado

	void activarRol(Integer codigo); // recuperar el objeto Rol, implementar el comportamiento de estado.

	void desactivarRol(Integer codigo); // recuperar el objeto Rol, imp

	List<UsuarioDTO> obtenerUsuarios(); // recuperar todos los usuarios

	void activarUsuario(String username); // recuperar el objeto Usuario, implementar el comportamiento de estado.

	void desactivarUsuario(String username); // recuperar el objeto Usuario, implementar el comportamiento de estado.
	
	//agregados por el grupo
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//CONFIGURACION
	
	Configuracion buscarConfiguracion (String nombre) throws ConnectionFailureException;// Busca configuracion por nombre
	
	List<ConfiguracionDTO> filtrarConfiguracionDTO(String query, List<ConfiguracionDTO> configuraciones) throws ConnectionFailureException;// Busca configuracion por nombre
	
	ConfiguracionDTO buscarConfiguracionDTO(String nombre) throws ConnectionFailureException;
		
	void altaConfiguracion(String nombre, String descripcion, List <String> listaParametros)
			throws DataEmptyException, RepeatedNameException, ConnectionFailureException;// Crea una nueva configuracion con lista de parametros

	void modificarConfiguracion(String nombre, String descripcionNueva, List<String> listaParametros) throws DataEmptyException, ConnectionFailureException;// modifica el nombre de la configuracion
	
	void agregarParametroaConfiguracion(String nombreConfiguracion,String nombreParametro) throws RepeatedNameException, ConnectionFailureException;// agrega uno o mas parametros a la configuracion
	
	void quitarParametroaConfiguracion (String parametro,String configuracion) throws ConnectionFailureException; // Quita aparametro a la configuracion
	
	public List<ConfiguracionDTO> getConfiguracionDTO() throws ConnectionFailureException;
	
	void eliminarConfiguracion(String nombre) throws ReferenceDataException, ConnectionFailureException;
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//PARAMETRO
	public void altaParametro(String nombre, List<String> unidades, String descripcion, String unidadPrincipal) throws DataEmptyException,RepeatedNameException, ConnectionFailureException;
	
	public void modificarParametro(String nombre, List<String> unidades, String descripcion, String unidadPrincipal)
			throws DataEmptyException,ConnectionFailureException;// podriamos hacer un metodo sobrecargado para modificar los atributos del parametro
	
	public void eliminarParametro(String nombre) throws ReferenceDataException, ConnectionFailureException;//elimina el parametro y el parametroDTO de sus respectivas listas
	
	Parametro buscarParametros(String nombre) throws ConnectionFailureException; // Metodo sobrecargado, se puede buscar por nombre
	
	ParametroDTO buscarParametroDTO(String nombre) throws ConnectionFailureException;
	
	public List<ParametroDTO> filtrarParametroDTO(String query, List<ParametroDTO> parametros) throws ConnectionFailureException;// filtra paramtroDTO por todos sus atributos

	public List<ParametroDTO> getParametroDTO() throws ConnectionFailureException;
	
	public Set<Parametro> obtenerParametros(List <String> lista) throws ConnectionFailureException;
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//UNIDAD DE MEDIDA
	
	List<String> obtenerSimbolosUnidadMedida(List<String> nombre) throws ConnectionFailureException;// busca unidades de medida por nombre y devuelve una lista de todos los simbolos de esa unidad
	
	List<String> obtenerTiposUnidadMedida(List<String> nombre) throws ConnectionFailureException;// busca unidades de medida por nombre y devuelve una lista de todos los tipos de dato de esa unidad
	
	void altaUnidadMedida (String nombre, String tipoDato, String simbolo) throws DataEmptyException, RepeatedNameException, ConnectionFailureException;
	
	Set <UnidadMedida> obtenerUnidadMedida(List <String> lista) throws ConnectionFailureException;

	List <UnidadMedidaDTO> getUnidadMedidaDTO() throws ConnectionFailureException;
	
	void eliminarUnidadeMedida(String unidadSeleccionada) throws ReferenceDataException, ConnectionFailureException;

	void modificarUnidadMedida(String nombre, String nuevoTipoDato, String nuevoSimbolo)throws DataEmptyException, ConnectionFailureException;

	UnidadMedida buscarUnidadMedida(String nombre) throws ConnectionFailureException;

	UnidadMedidaDTO buscarUnidadMedidaDTO(String nombre) throws ConnectionFailureException;
	
	List<String> listarTiposDato();

	List<UnidadMedidaDTO> filtrarUnidadMedida(String query, List<UnidadMedidaDTO> unidades);
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//DISPOSITIVO
	
	Dispositivo devolverDispositivo(String nombre) throws ConnectionFailureException;

	void desactivarDispositivo(String nombreDispositivoDTO) throws ConnectionFailureException;

	List<DispositivoDTO> getDispositivosDTO() throws ConnectionFailureException;

	void activarDispositivo(String nombreDispositivoDTO) throws ConnectionFailureException;

	DispositivoDTO recuperarDispositivoDTO(String nombreDispositivo) throws ConnectionFailureException;

	void altaDispositivo(String nombre, String descripcion, String modelo, String marca, boolean estado,String nombreConfig)
			throws DataEmptyException, ConnectionFailureException, RepeatedNameException;
	
	public List<DispositivoDTO> ordenarPorNombreAscendente(List<DispositivoDTO> lista) throws ConnectionFailureException;
	
	public void eliminarDispositivo(String nombreDispositivo) throws ConnectionFailureException;
	
	public void modificarDispositivo(String nombreDispositivo, String nuevaDescripcion, String nuevoModelo, String nuevaMarca, boolean nuevoEstado,String nombreConfig)
			throws DataEmptyException, ConnectionFailureException;
	
	public void existeDispositivo(String nombreDispositivo) throws ConnectionFailureException, RepeatedNameException;

	List<DispositivoDTO> filtrarDispositivos(String query, List<DispositivoDTO> dispositivos);
}
