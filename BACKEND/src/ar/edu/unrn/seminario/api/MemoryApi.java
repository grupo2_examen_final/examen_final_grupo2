package ar.edu.unrn.seminario.api;
import ar.edu.unrn.seminario.exception.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.dto.RolDTO;
import ar.edu.unrn.seminario.dto.UnidadMedidaDTO;
import ar.edu.unrn.seminario.dto.UsuarioDTO;
import ar.edu.unrn.seminario.modelo.Configuracion;
import ar.edu.unrn.seminario.modelo.Parametro;
import ar.edu.unrn.seminario.modelo.Rol;
import ar.edu.unrn.seminario.modelo.Usuario;
import ar.edu.unrn.seminario.modelo.UnidadMedida;
import ar.edu.unrn.seminario.modelo.Dispositivo;

public class MemoryApi implements IApi
{

	private Map<Integer, Rol> roles = new HashMap<>();
	private Set<Usuario> usuarios = new HashSet<>();
	private Set<UnidadMedida> listaUnidadMedida = new HashSet<>();
	private Set <Configuracion> listaConfiguracion = new HashSet<>();
	private Set<Parametro> listaParametros= new HashSet<>();
	private List<String> listaTiposDato = new ArrayList<>();
	private Set<Dispositivo> listaDispositivos = new HashSet<>();



	public MemoryApi()
	{
		
	}

	private void inicializarUsuarios() {
		registrarUsuario("mcambarieri", "1234", "mcambarieri@unrn.edu.ar", "Mauro", 3);
		registrarUsuario("ldifabio", "1234", "ldifabio@unrn.edu.ar", "Lucas", 2);
		registrarUsuario("admin", "1234", "admin@unrn.edu.ar", "Admin", 1);

	}

	@Override
	public void registrarUsuario(String username, String password, String email, String nombre, Integer rol)
	{
		Rol role = this.roles.get(rol);
		Usuario usuario = new Usuario(username, password, nombre, email, role);
		this.usuarios.add(usuario);
	}



	@Override
	public UsuarioDTO obtenerUsuario(String username) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void eliminarUsuario(String username) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public List<RolDTO> obtenerRoles()
	{
		List<RolDTO> dtos = new ArrayList<>();
		for (Rol r : this.roles.values()) {
			dtos.add(new RolDTO(r.getCodigo(), r.getNombre()));
		}
		return dtos;
	}



	@Override
	public List<RolDTO> obtenerRolesActivos()
	{
		List<RolDTO> dtos = new ArrayList<>();
		for (Rol r : this.roles.values()) {
			if (r.isActivo())
				dtos.add(new RolDTO(r.getCodigo(), r.getNombre()));
		}
		return dtos;
	}



	@Override
	public void guardarRol(Integer codigo, String descripcion, boolean estado)
	{
		Rol rol = new Rol(codigo, descripcion);
		this.roles.put(codigo, rol);
	}


	@Override
	public RolDTO obtenerRolPorCodigo(Integer codigo) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void activarRol(Integer codigo) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void desactivarRol(Integer codigo) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public List<UsuarioDTO> obtenerUsuarios()
	{
		List<UsuarioDTO> dtos = new ArrayList<>();
		for (Usuario u : this.usuarios) {
			dtos.add(new UsuarioDTO(u.getUsuario(), u.getContrasena(), u.getNombre(), u.getEmail(),
					u.getRol().getNombre(), u.isActivo(), u.obtenerEstado()));
		}
		return dtos;
	}



	@Override
	public void activarUsuario(String usuario)
	{
		for (Usuario u : usuarios) {
			if (u.getUsuario().equals(usuario))
				u.activar();
			// enviar mail
			// ..
		}
	}



	@Override
	public void desactivarUsuario(String usuario)
	{
		for (Usuario u : usuarios) {
			if (u.getUsuario().equals(usuario))
				u.desactivar();

		}
	}

	///////////////////////////////////////////////////////////////////////
	//CONFIGURACION


	@Override
	public Configuracion buscarConfiguracion(String nombre)
	{
		Configuracion temp = new Configuracion();
		for (Configuracion c : listaConfiguracion) {
			
			if (c.getNombre().equalsIgnoreCase(nombre)) {
				temp=c;
			}
		}
		return temp;
	}



	@Override
	public List<ConfiguracionDTO> filtrarConfiguracionDTO(String query, List<ConfiguracionDTO> configuraciones)
	{
		return configuraciones.stream()
				.filter((unidad) -> unidad.getNombre().toLowerCase().contains(query.toLowerCase()) || unidad.getDescripcion().equalsIgnoreCase(query))
				.sorted((unidad1, unidad2) -> unidad1.getNombre().compareToIgnoreCase(unidad2.getNombre()))
				.collect(Collectors.toList());
	}



	@Override
	public ConfiguracionDTO buscarConfiguracionDTO(String nombre)
	{
		ConfiguracionDTO config = new ConfiguracionDTO(buscarConfiguracion(nombre));
		return config;
	}

	


	@Override
	public void altaConfiguracion(String nombre, String descripcion, List<String> listaParametros)throws DataEmptyException, RepeatedNameException
	{
        Set <Parametro> listaP = obtenerParametros(listaParametros);
        Configuracion nuevaConfig= new Configuracion (nombre, descripcion, listaP);
        if (!listaConfiguracion.contains(nuevaConfig)){
        	listaConfiguracion.add(nuevaConfig);
        }  
	}



	@Override
	public void modificarConfiguracion(String nombre, String descripcionNueva, List<String> listaParametros) throws DataEmptyException
	{
	    Iterator<Configuracion> iterator = listaConfiguracion.iterator();
	    
	    Set<Parametro> parametros = obtenerParametros(listaParametros);
	    
	    Configuracion config = new Configuracion(nombre, descripcionNueva, parametros);
	    
	    while (iterator.hasNext() ) {
	    	Configuracion configuracion = iterator.next();
	        if (configuracion.getNombre().equalsIgnoreCase(nombre)) {
	        	iterator.remove();
	        }
	    }
	    
	    listaConfiguracion.add(config);
	}



	@Override
	public void agregarParametroaConfiguracion(String nombreConfiguracion, String nombreParametro)throws RepeatedNameException
	{
		for (Configuracion config : this.listaConfiguracion) {
			if (config.getNombre().equalsIgnoreCase(nombreConfiguracion)) {
				config.agregarParametro(this.buscarParametros(nombreParametro));
			}
		}
	}



	@Override
	public void quitarParametroaConfiguracion(String nombreConfiguracion, String nombreParametro)
	{
		for (Configuracion config : this.listaConfiguracion) {
			if (config.getNombre().equalsIgnoreCase(nombreConfiguracion)) {
				config.eliminarParametro(nombreParametro);			
			}
		}
	}






	@Override
	public List<ConfiguracionDTO> getConfiguracionDTO()
	{
		List<ConfiguracionDTO> listaDTO = new ArrayList<>();
		for(Configuracion config : listaConfiguracion)
		{
			ConfiguracionDTO configDTO = new ConfiguracionDTO(config);
			listaDTO.add(configDTO);
		}
		return listaDTO;
	}


	@Override
	public void eliminarConfiguracion(String nombre) throws ReferenceDataException
	{
		Configuracion config=buscarConfiguracion(nombre);
		System.out.println(config.getNombre());
		
		for (Dispositivo dispositivo : listaDispositivos)
		{
			if (dispositivo.getConfiguracion().equals(config))
			{
				throw new ReferenceDataException ("No se puede eliminar. La configuracion es siendo usada en algun dispositivo");
			}
		}
		
		listaConfiguracion.remove(config);
	}

	///////////////////////////////////////////////////////////////////////
	//PARAMETRO

	@Override
	public void altaParametro(String nombre, List<String> unidades, String descripcion, String unidadPrincipal) throws DataEmptyException,RepeatedNameException
	{
		Set<UnidadMedida> unidad = obtenerUnidadMedida(unidades);		
		UnidadMedida uPrincipal = buscarUnidadMedida(unidadPrincipal);
		
		Parametro parametroNuevo = new Parametro(nombre, unidad, descripcion, uPrincipal);
		if(!listaParametros.contains(parametroNuevo))
		{
			listaParametros.add(parametroNuevo);
		}
	}



	@Override
	public void modificarParametro(String nombre, List<String> unidades, String descripcion, String unidadPrincipal)
			throws DataEmptyException
	{
		Set<UnidadMedida> unidad = obtenerUnidadMedida(unidades);
		List<UnidadMedidaDTO> listUPrincipal = filtrarUnidadMedida(unidadPrincipal, getUnidadMedidaDTO());
		UnidadMedida uPrincipal = new UnidadMedida(listUPrincipal.get(0).getNombre(), listUPrincipal.get(0).getTipoDato(), listUPrincipal.get(0).getSimbolo());
		
	    Iterator<Parametro> iterator = listaParametros.iterator();

	    Parametro parametroModificado = new Parametro(nombre, unidad, descripcion, uPrincipal);
	    while (iterator.hasNext())
    	    {
    		    Parametro parametro = iterator.next();       
    	        if (parametro.getNombre().equalsIgnoreCase(nombre))
    	        {
    	        	iterator.remove();
    	        }
    	    }

	    listaParametros.add(parametroModificado);
	}


	@Override
	public void eliminarParametro(String nombre) throws ReferenceDataException
	{
		Parametro parametroEliminar = buscarParametros(nombre);
		
		for(Configuracion config : listaConfiguracion)
		{
			if(config.getListaParametros().contains(parametroEliminar))
			{
				throw new ReferenceDataException ("No se puede eliminar. El parametro esta siendo usada en alguna configuracion");
			}
		}
		
		if(listaParametros.contains(parametroEliminar))
		{
			listaParametros.remove(parametroEliminar);
		}
	}



	@Override
	public Parametro buscarParametros(String nombre)
	{
		Parametro param = null;
		for (Parametro p : listaParametros) {
			if (p.getNombre().equalsIgnoreCase(nombre)) {
				param = p;	
			}
		}
		return param;
	}

	
	public ParametroDTO buscarParametroDTO(String nombre)
	{
		ParametroDTO param=new ParametroDTO(buscarParametros(nombre));
		return param;
	}


	@Override
	public List<ParametroDTO> filtrarParametroDTO(String query, List<ParametroDTO> parametros)
	{
		return parametros.stream()
				.filter((unidad) -> unidad.getUnidadPrincipal().toLowerCase().contains(query.toLowerCase()) || unidad.getDescripcion().toLowerCase().contains(query.toLowerCase()) || unidad.getNombre().toLowerCase().contains(query.toLowerCase()))//agregar demas filtros
				.sorted((unidad1, unidad2) -> unidad1.getNombre().compareToIgnoreCase(unidad2.getNombre()))
				.collect(Collectors.toList());
	}

	@Override
	public List<ParametroDTO> getParametroDTO()
	{
		List<ParametroDTO> listaParametrosDTO = new ArrayList<>();
		
		for(Parametro parametro : listaParametros)
		{
			ParametroDTO parametroDTO = new ParametroDTO(parametro);
			listaParametrosDTO.add(parametroDTO);
		}
		
		return listaParametrosDTO;
	}


	@Override
	public Set<Parametro> obtenerParametros(List<String> lista)
	{
		Set<Parametro> listaP = new HashSet<>();
		for (String texto : lista)
		{
			for (Parametro p : listaParametros)
			{
				if (p.getNombre().equalsIgnoreCase(texto))
				{
					listaP.add(p);
				}
			}
		}
		return listaP;
	}

	///////////////////////////////////////////////////////////////////////
	//UNIDAD DE MEDIDA
	
	@Override
	public List<String> obtenerSimbolosUnidadMedida(List<String> nombre)
	{
		List<String> listaSimbolos = new ArrayList<>();
		for (String texto : nombre)
		{
			for (UnidadMedida unidades : listaUnidadMedida)
				{
					if(unidades.getNombre().equals(texto))
					{
						listaSimbolos.add(unidades.getSimbolo());
					}
				}
		}
		return listaSimbolos;
	}



	@Override
	public List<String> obtenerTiposUnidadMedida(List<String> nombre)
	{
		List<String> listaSimbolos = new ArrayList<>();
		for (UnidadMedida unidades : listaUnidadMedida)
		{
			if(unidades.getNombre().equals(nombre))
			{
				listaSimbolos.add(unidades.getTipoDato());
			}
		}
		return listaSimbolos;
	}
	
	@Override
	public void altaUnidadMedida(String nombre, String tipoDato, String simbolo)
			throws DataEmptyException, RepeatedNameException {
		UnidadMedida unidadMedida = new UnidadMedida(nombre, tipoDato, simbolo);
		if(listaUnidadMedida.contains(unidadMedida)){
			throw new RepeatedNameException("Ya existe una unidad de medida con ese nombre");
		}
		else {
			listaUnidadMedida.add(unidadMedida);
		}
		
	}

	@Override
	public Set<UnidadMedida> obtenerUnidadMedida(List<String> lista) {
		Set<UnidadMedida> nuevaLista = new HashSet<>();
		for (String unidad : lista)
		{
			for(UnidadMedida unidadPura : listaUnidadMedida)
			{
				if(unidadPura.getNombre().equals(unidad))
				{
					nuevaLista.add(unidadPura);
				}
			}
		}
		
		return nuevaLista;
	}

	@Override
	public List<UnidadMedidaDTO> getUnidadMedidaDTO() {
		List<UnidadMedidaDTO> listaUnidadMedidaDTO = new ArrayList<>();
		for (UnidadMedida unidad : listaUnidadMedida) {
			UnidadMedidaDTO unidadDTO = new UnidadMedidaDTO(unidad);
			listaUnidadMedidaDTO.add(unidadDTO);
		}
		return listaUnidadMedidaDTO;
	}

	@Override
	public void eliminarUnidadeMedida(String unidadSeleccionada) throws ReferenceDataException {
		UnidadMedida unidadMedida = new UnidadMedida();
		unidadMedida.setNombre(unidadSeleccionada);
		for (Parametro parametro : listaParametros)
		{	
			if(parametro.getUnidadMedida().contains(unidadMedida))
			{
				throw new ReferenceDataException("Para eliminar esta unidad primero debe quitar sus referencias");
			}
		}
		listaUnidadMedida.remove(unidadMedida);
	}

	@Override
	public void modificarUnidadMedida(String nombre, String nuevoTipoDato, String nuevoSimbolo)
			throws DataEmptyException {
		for(UnidadMedida unidad : listaUnidadMedida) {
			if (unidad.getNombre().equals(nombre)) {
				unidad.setTipoDato(nuevoTipoDato);
				unidad.setSimbolo(nuevoSimbolo);
			}
		}
	}

	@Override
	public UnidadMedida buscarUnidadMedida(String nombre) {
		UnidadMedida unidadBuscada = null;
		for	(UnidadMedida unidad : listaUnidadMedida) {
			if (unidad.getNombre().equalsIgnoreCase(nombre)){
				unidadBuscada = unidad;
			}
		}
		return unidadBuscada;
	}
	
	@Override
	public UnidadMedidaDTO buscarUnidadMedidaDTO(String nombre) {
		UnidadMedidaDTO unidadBuscada = new UnidadMedidaDTO(buscarUnidadMedida(nombre));
		return unidadBuscada;
	}


	@Override
	public List<String> listarTiposDato() {
		List<String> listaTiposDato = new ArrayList<>();
		listaTiposDato.add("Short");
		listaTiposDato.add("Integer");
		listaTiposDato.add("Long");
		listaTiposDato.add("Float");
		listaTiposDato.add("Double");
		listaTiposDato.add("Character");
	
		return listaTiposDato;
	}

	@Override
	public List<UnidadMedidaDTO> filtrarUnidadMedida(String query, List<UnidadMedidaDTO> unidades) {
		return unidades.stream()
				.filter((unidad) -> unidad.getNombre().toLowerCase().contains(query.toLowerCase()) || unidad.getTipoDato().equalsIgnoreCase(query) || unidad.getSimbolo().equalsIgnoreCase(query))
				.sorted((unidad1, unidad2) -> unidad1.getNombre().compareToIgnoreCase(unidad2.getNombre()))
				.collect(Collectors.toList());
		}
	
	
	///////////////////////////////////////////////////////////////////////
	//UNIDAD DE MEDIDA
	
	@Override
	public void altaDispositivo(String nombre, String descripcion, String modelo, String marca, boolean estado,String nombreConfiguracion) throws RepeatedNameException, DataEmptyException{
		Configuracion config = buscarConfiguracion(nombreConfiguracion);
		Dispositivo dispositivo = new Dispositivo(nombre, descripcion, modelo, marca, estado,config);
		existeDispositivo(nombre);
		this.listaDispositivos.add(dispositivo);
	}
	
	@Override
	public DispositivoDTO recuperarDispositivoDTO(String nombreDispositivo) {
		Dispositivo dispositivo =  listaDispositivos.stream()
			.filter((p) -> p.getNombre() == nombreDispositivo)
			.findFirst().get();
		DispositivoDTO dispositivoDTO = new DispositivoDTO(dispositivo);
		return dispositivoDTO;
	}
	
	@Override
	public void activarDispositivo(String nombreDispositivoDTO) {
		devolverDispositivo(recuperarDispositivoDTO(nombreDispositivoDTO).getNombre()).activar();
	}

	@Override
	public void desactivarDispositivo(String nombreDispositivoDTO) {
		devolverDispositivo(recuperarDispositivoDTO(nombreDispositivoDTO).getNombre()).desactivar();
	}
	
	@Override
	public List<DispositivoDTO> getDispositivosDTO() {
		List<DispositivoDTO> dtos = new ArrayList<>();
		for (Dispositivo u : this.listaDispositivos) {
			dtos.add(new DispositivoDTO(u));	
		}
		return dtos;
	}
	
	@Override
	public Dispositivo devolverDispositivo(String nombre) {
		Dispositivo dispositivo =  listaDispositivos.stream()
				.filter((p) -> p.getNombre() == nombre)
				.findFirst().get();
		return dispositivo;
	}
	
	@Override
	public void existeDispositivo(String nombreDispositivo) throws RepeatedNameException {

		for(Dispositivo dispositivo : listaDispositivos) {
			if(dispositivo.getNombre().equals(nombreDispositivo)) {
				throw new RepeatedNameException("Ya existe un dispositivo con ese nombre");
			}
		}
	}
	@Override
	public void modificarDispositivo(String nombreDispositivo, String nuevaDescripcion, String nuevoModelo, String nuevaMarca, boolean nuevoEstado,String configuracion)  throws DataEmptyException{
		Iterator<Dispositivo> iterator = listaDispositivos.iterator();
		Configuracion config = buscarConfiguracion(configuracion);
		Dispositivo dispositivoNuevo = new Dispositivo (nombreDispositivo,nuevaDescripcion,nuevaMarca,nuevoModelo,nuevoEstado,config);
	    while (iterator.hasNext() ) {
		    Dispositivo dispositivo = iterator.next();       
	        if (dispositivo.getNombre().equalsIgnoreCase(nombreDispositivo)) {
	        	iterator.remove();
	        }
	    }

		    listaDispositivos.add(dispositivoNuevo);
		}
	@Override
	public void eliminarDispositivo(String nombreDispositivo) {
		Dispositivo dispositivo = devolverDispositivo(nombreDispositivo);
		listaDispositivos.remove(dispositivo);
	}
	
	
	@Override
	public List<DispositivoDTO> ordenarPorNombreAscendente(List<DispositivoDTO> lista) {
		lista = lista.stream()
				.sorted((p1, p2) ->
				{
					return(p1.getNombre().compareToIgnoreCase(p2.getNombre()));
				})
				.collect(Collectors.toList());
		return lista;
	}

	@Override
	public List<DispositivoDTO> filtrarDispositivos(String query, List<DispositivoDTO> dispositivos) {
		// TODO Auto-generated method stub
		return null;
	}
}
	
