package ar.edu.unrn.seminario.dto;

import ar.edu.unrn.seminario.modelo.UnidadMedida;

public class UnidadMedidaDTO
{
	private String nombre;
	private String tipoDato;
	private String simbolo;

	public UnidadMedidaDTO() {
		
	}
	
	public UnidadMedidaDTO(UnidadMedida unidadMedida)
	{
		this.nombre = unidadMedida.getNombre();
		this.tipoDato = unidadMedida.getTipoDato();
		this.simbolo = unidadMedida.getSimbolo();
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getTipoDato() {
		return tipoDato;
	}
	
	public String getSimbolo() {
		return simbolo;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}
	
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
}
