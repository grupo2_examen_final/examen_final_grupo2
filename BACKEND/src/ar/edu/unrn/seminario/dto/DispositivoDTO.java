package ar.edu.unrn.seminario.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.modelo.Parametro;

public class DispositivoDTO{
	
	//espacio publico
	
	private String nombre;
	private String descripcion;
	private String modelo;
	private String marca;
	private boolean estado;
	private String configuracion;
	private List <String> listaParametros= new ArrayList<String>();
	
	public List<String> getListaParametros() {
		return listaParametros;
	}

	public void setListaParametros(List<String> listaParametros) {
		this.listaParametros = listaParametros;
	}

	public DispositivoDTO(Dispositivo dispositivo) {
		this.estado = dispositivo.getEstado();
		this.nombre = dispositivo.getNombre();
		this.descripcion = dispositivo.getDescripcion();
		this.modelo = dispositivo.getModelo();
		this.marca = dispositivo.getMarca();
		this.configuracion= dispositivo.getConfiguracion().getNombre();
		Set <Parametro>lista= dispositivo.getConfiguracion().getListaParametros();
		for (Parametro parametro : lista) {
			listaParametros.add(parametro.getNombre());
		}
	}

	public boolean isEstado() {
		return estado;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getModelo() {
		return modelo;
	}

	public String getMarca() {
		return marca;
	}

	public String getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(String configuracion) {
		this.configuracion = configuracion;
	}

}