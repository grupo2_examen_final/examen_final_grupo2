package ar.edu.unrn.seminario.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.modelo.Configuracion;
import ar.edu.unrn.seminario.modelo.Parametro;

public class ConfiguracionDTO {
	private String nombre;
	private String descripcion;
	private List <String> listaParametros=new ArrayList<>();
	
	
	public ConfiguracionDTO () {
		
	}
	
	
	
	public ConfiguracionDTO(Configuracion config) {
		this.descripcion= config.getDescripcion();
		this.nombre=config.getNombre();
		List <String> lista= new ArrayList<String>();
		Set <Parametro> listaTemp= config.getListaParametros();
		for(Parametro p : listaTemp) {
			lista.add(p.getNombre());
		}
		this.listaParametros=lista;
		
	}
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public List <String> getListaParametros() {
		return listaParametros;
	}
	public void setListaParametros(List <String> listaParametros) {
		this.listaParametros = listaParametros;
	}
	
	public void eliminarParametro(String Nombre) {
		Iterator<String> iterador = this.listaParametros.iterator();
	    while (iterador.hasNext()) {
	        String parametro = iterador.next();
	        if (parametro.equalsIgnoreCase(Nombre)) {
	            iterador.remove();
	        }
	    }
	}
	    
	    public void agregarParametro (String Nombre) {
	    	this.listaParametros.add(Nombre);
	    }
	    
	    
	public void eliminarTodosLosParametros() {
		this.listaParametros.clear();
	}
}
