package ar.edu.unrn.seminario.dto;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;

import ar.edu.unrn.seminario.modelo.Parametro;
import ar.edu.unrn.seminario.modelo.UnidadMedida;

public class ParametroDTO
{
	private String nombre;
	private List<String> unidadMedida;
	private String descripcion;
	private String unidadPrincipal;
	
	
	public ParametroDTO()
	{
		
	}
	
	public ParametroDTO(Parametro parametro)
	{
		unidadMedida=new ArrayList<String>();
		this.nombre = parametro.getNombre();
		this.descripcion = parametro.getDescripcion();
		this.unidadPrincipal = parametro.getUnidadPrincipal().getNombre();
		Set <UnidadMedida> listaTemp = parametro.getUnidadMedida();
		for (UnidadMedida u : listaTemp) {
			this.unidadMedida.add(u.getNombre());
		}
	}

	public ParametroDTO(String nombre, List<String> unidadMedida, String descripcion, String unidadPrincipal)
	{
		this.nombre = nombre;
		this.unidadMedida = unidadMedida;
		this.descripcion = descripcion;
		this.unidadPrincipal = unidadPrincipal;
	}

	public String getNombre() {
		return nombre;
	}

	public List<String> getUnidadMedida() {
		return unidadMedida;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getUnidadPrincipal() {
		return unidadPrincipal;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setUnidadMedida(List<String> unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setUnidadPrincipal(String unidadPrincipal) {
		this.unidadPrincipal = unidadPrincipal;
	}
}
