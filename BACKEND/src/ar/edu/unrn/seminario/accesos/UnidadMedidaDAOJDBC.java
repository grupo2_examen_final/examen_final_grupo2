package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ar.edu.unrn.seminario.api.UnidadMedidaDAO;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.modelo.UnidadMedida;

public class UnidadMedidaDAOJDBC implements UnidadMedidaDAO {
	private final static String conexion = "jdbc:mysql://localhost:3306/gepdm_2grupo";
	private final static String usuario= "Tonhy";
	private final static String clave = "@i)[Fk8Z8p-Ho5Rb";

	@Override
	public void create(UnidadMedida unidad) throws ConnectionFailureException {
	    String consultaSQL = "INSERT INTO unidadesmedida (nombre, simbolo, tipoDato) VALUES (?, ?, ?)";
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    		PreparedStatement statement = myConexion.prepareStatement(consultaSQL)) {
		     statement.setString(1, unidad.getNombre());
		     statement.setString(2, unidad.getSimbolo());
		     statement.setString(3, unidad.getTipoDato());
		     statement.executeUpdate();
	        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}
	    
	@Override
	public void update(UnidadMedida unidad) throws ConnectionFailureException{
	    String consultaSQL = "UPDATE unidadesmedida SET simbolo = ?, tipoDato = ? WHERE nombre = ?";
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave); 
	    		PreparedStatement statement = myConexion.prepareStatement(consultaSQL)){
	    	statement.setString(1, unidad.getSimbolo());
	        statement.setString(2, unidad.getTipoDato());
	        statement.setString(3, unidad.getNombre());
	        statement.executeUpdate();
	        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}
	  
	@Override
	public void remove(String nombre) throws ConnectionFailureException{

		String consultaSQL = "DELETE FROM unidadesmedida WHERE nombre = ?";
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave); 	
	    	PreparedStatement statement = myConexion.prepareStatement(consultaSQL)) {
	        statement.setString(1, nombre);
	        statement.executeUpdate();
	        } 
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}
	
	@Override
	public UnidadMedida find(String nombre) throws ConnectionFailureException{
	    UnidadMedida unidadEncontrada = null;
	    String consultaSQL = "SELECT * FROM unidadesmedida WHERE nombre = ?";
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    		PreparedStatement statement = myConexion.prepareStatement(consultaSQL)) {
	    	statement.setString(1, nombre);
	    	try (ResultSet resultSet = statement.executeQuery()){
	    		if (resultSet.next()) {
	    			unidadEncontrada = new UnidadMedida();
	    			unidadEncontrada.setNombre(resultSet.getString("nombre"));
	    			unidadEncontrada.setTipoDato(resultSet.getString("tipoDato"));
	    			unidadEncontrada.setSimbolo(resultSet.getString("simbolo"));
	    		}
	    	} 
		    catch (SQLException S) {
	        	throw new ConnectionFailureException("Error en la base de datos");
		    }    	
	    }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return unidadEncontrada;
	}

	@Override
	public Set<UnidadMedida> findAll() throws ConnectionFailureException{
	    Set<UnidadMedida> listaUnidades = new HashSet<>();  
	    String consultaSQL = "SELECT * FROM unidadesmedida";
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    		PreparedStatement statement = myConexion.prepareStatement(consultaSQL);
	            ResultSet resultSet = statement.executeQuery()) {
	    	while (resultSet.next()) {
	    		UnidadMedida unidad = new UnidadMedida();
	            unidad.setNombre(resultSet.getString("nombre"));
	            unidad.setTipoDato(resultSet.getString("tipoDato"));
	            unidad.setSimbolo(resultSet.getString("simbolo"));
	            listaUnidades.add(unidad);
	            }
	        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return listaUnidades;
	}
}
