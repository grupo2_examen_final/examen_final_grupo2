package ar.edu.unrn.seminario.accesos;

import java.sql.Connection;

import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import ar.edu.unrn.seminario.api.ConfiguracionDAO;
import ar.edu.unrn.seminario.api.UnidadMedidaDAO;
import ar.edu.unrn.seminario.modelo.Configuracion;
import ar.edu.unrn.seminario.modelo.Parametro;




public class ConfiguracionDAOJDBC implements ConfiguracionDAO {
	private final static String conexion = "jdbc:mysql://localhost:3306/gepdm_2grupo";
	private final static String usuario= "Tonhy";
	private final static String clave = "@i)[Fk8Z8p-Ho5Rb";
	private UnidadMedidaDAO unidadDAO = new UnidadMedidaDAOJDBC();

	@Override

	public void create(Configuracion config) throws ConnectionFailureException{
	    String insertar = "INSERT INTO configuraciones (nombre, descripcion) VALUES (?, ?)";
        String tablaIntermedia = "INSERT INTO configuracion_parametro (nombreConfiguracion,nombreParametro) VALUES (?, ?)";


	    Set <Parametro> listaParametros= config.getListaParametros();

	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    	PreparedStatement statementInsertar = myConexion.prepareStatement(insertar);
	    	PreparedStatement statementTablaItermedia = myConexion.prepareStatement(tablaIntermedia))
	    		{
	    		statementInsertar.setString(1, config.getNombre());
	    		statementInsertar.setString(2, config.getDescripcion());
	    		statementInsertar.executeUpdate();
	    		
	    		for (Parametro p : listaParametros) {
	    			statementTablaItermedia.setString(1,config.getNombre());
	    			statementTablaItermedia.setString(2,p.getNombre());
	    			statementTablaItermedia.executeUpdate();
	    			}
	    }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}

	@Override
	public void update(Configuracion config) throws ConnectionFailureException {
		 Set <Parametro> listaParametro = config.getListaParametros();
         String delete = "DELETE FROM  configuracion_parametro WHERE nombreConfiguracion= ?";
         String update = "UPDATE configuraciones SET descripcion = ? WHERE nombre = ?";
         String tablaIntermedia = "INSERT INTO configuracion_parametro (nombreConfiguracion,nombreParametro) VALUES (?, ?)";
     		 
		 try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave); 
			PreparedStatement statementBorrarRelacion = myConexion.prepareStatement(delete);
			PreparedStatement statementUpdate = myConexion.prepareStatement(update);
			PreparedStatement statementInsertarRelacion = myConexion.prepareStatement(tablaIntermedia))		 
			{		 
				statementBorrarRelacion.setString(1, config.getNombre());
				statementBorrarRelacion.executeUpdate();
				
				statementUpdate.setString(1, config.getDescripcion());
				statementUpdate.setString(2, config.getNombre());
				statementUpdate.executeUpdate();
				
				 for (Parametro p : listaParametro) {
						statementInsertarRelacion.setString(1,config.getNombre());
						statementInsertarRelacion.setString(2,p.getNombre());
						statementInsertarRelacion.executeUpdate();	
			        	} 
		    }
		    catch (SQLException S) {
	        	throw new ConnectionFailureException("Error en la base de datos");
		    }
	}
	
	
	@Override
	public void remove(String nombre) throws ConnectionFailureException {
		String deleteConfig = "DELETE FROM configuraciones WHERE nombre = ?";
        String deletetablaIntermedia = "DELETE FROM configuracion_parametro WHERE nombreConfiguracion = ?";

		
		try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
			PreparedStatement borrarConfigTablaIntermedia = myConexion.prepareStatement(deletetablaIntermedia);
			PreparedStatement borrarConfiguracion = myConexion.prepareStatement(deleteConfig))
			{
			
			borrarConfigTablaIntermedia.setString(1, nombre);
			borrarConfigTablaIntermedia.executeUpdate();
			
			borrarConfiguracion.setString(1, nombre);
			borrarConfiguracion.executeUpdate();
			
		}
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}

	@Override
	public Configuracion find(String nombre) throws ConnectionFailureException {
	    Configuracion configEcontrada = new Configuracion();
	    String buscar = "SELECT * FROM configuraciones WHERE nombre = ?";
	    String busquedaParametros = "SELECT p.nombre, p.descripcion, p.unidadPrincipal FROM configuracion_parametro cp JOIN parametros p ON cp.nombreParametro = p.nombre  WHERE cp.nombreConfiguracion = ?";

	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    	PreparedStatement statement = myConexion.prepareStatement(buscar);
	    	PreparedStatement buscarParametros = myConexion.prepareStatement(busquedaParametros))
	    	{
    		Set<Parametro> listaParametros = new HashSet<>();
    		
	    	statement.setString(1, nombre);
	    	buscarParametros.setString(1, nombre);
	    	
	    	try (ResultSet resultadoBusqueda = statement.executeQuery();
	    		ResultSet resultBusquedaParametros = buscarParametros.executeQuery())
	    	{
	    		 if (resultadoBusqueda.next()) {
	 	        	configEcontrada.setNombre(resultadoBusqueda.getString("nombre"));
	 	        	configEcontrada.setDescripcion(resultadoBusqueda.getString("descripcion"));
	 	        	buscarParametros.setString(1, configEcontrada.getNombre());
 	            	while (resultBusquedaParametros.next()) {
 		            	Parametro parametro= new Parametro();
 		            	parametro.setNombre(resultBusquedaParametros.getString("nombre"));
 		            	parametro.setDescripcion(resultBusquedaParametros.getString("descripcion"));
 		                parametro.setUnidadPrincipal(unidadDAO.find(resultBusquedaParametros.getString("unidadPrincipal")));
 		                listaParametros.add(parametro);
 		            }
	 	        	configEcontrada.setListaParametros(listaParametros);
	 	            }
	    	}
	    }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return configEcontrada;
	}

	@Override
	public Set<Configuracion> findAll() throws ConnectionFailureException{
	    Set<Configuracion> listaConfiguracion = new HashSet<>();
	    String busqueda = "SELECT * FROM configuraciones";
	    String busquedaParametros = "SELECT p.nombre, p.descripcion, p.unidadPrincipal FROM configuracion_parametro cp JOIN parametros p ON cp.nombreParametro = p.nombre  WHERE cp.nombreConfiguracion = ?";

	    
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    		PreparedStatement buscarConfiguracion = myConexion.prepareStatement(busqueda);
	    		PreparedStatement buscarParametros = myConexion.prepareStatement(busquedaParametros);
	    		ResultSet resultBusquedaConfiguracion = buscarConfiguracion.executeQuery()) {	    	
	    		Set<Parametro> listaParametros = new HashSet<>();

	        while (resultBusquedaConfiguracion.next()) {            
	    	    Configuracion configuracion = new Configuracion();
	    	    configuracion.setNombre(resultBusquedaConfiguracion.getString("nombre"));
	            configuracion.setDescripcion(resultBusquedaConfiguracion.getString("descripcion"));
	            buscarParametros.setString(1, configuracion.getNombre());
	            listaParametros.clear();
	            try (ResultSet resultBusquedaParametros = buscarParametros.executeQuery()){
	            	while (resultBusquedaParametros.next()) {
		            	Parametro parametro= new Parametro();
		            	parametro.setNombre(resultBusquedaParametros.getString("nombre"));
		            	parametro.setDescripcion(resultBusquedaParametros.getString("descripcion"));
		                parametro.setUnidadPrincipal(unidadDAO.find(resultBusquedaParametros.getString("unidadPrincipal")));
		                listaParametros.add(parametro);
		            }
	            }
            	listaConfiguracion.add(configuracion);
            	configuracion.setListaParametros(listaParametros);
	        }
	    }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return listaConfiguracion;
	}

}
