package ar.edu.unrn.seminario.accesos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import ar.edu.unrn.seminario.api.DispositivoDAO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.modelo.Configuracion;
import ar.edu.unrn.seminario.modelo.Dispositivo;
import ar.edu.unrn.seminario.api.ConfiguracionDAO;


public class DispositivoDAOJDBC implements DispositivoDAO {
	private final static String conexion = "jdbc:mysql://localhost:3306/gepdm_2grupo";
	private final static String usuario= "Tonhy";
	private final static String clave = "@i)[Fk8Z8p-Ho5Rb";
	private ConfiguracionDAO configDAO = new ConfiguracionDAOJDBC();
	
	
	
	
	@Override
	public void create(Dispositivo dispositivo) throws ConnectionFailureException{
	String insertar = "INSERT INTO dispositivos (estado, nombre,descripcion,modelo,marca,configuracion) VALUES (?, ?, ?, ?, ?, ?)";	
    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
    	PreparedStatement statementInsertar = myConexion.prepareStatement(insertar))
    		{
    		statementInsertar.setBoolean(1, dispositivo.getEstado());
    		statementInsertar.setString(2, dispositivo.getNombre());
    		statementInsertar.setString(3, dispositivo.getDescripcion());
    		statementInsertar.setString(4, dispositivo.getModelo());
    		statementInsertar.setString(5, dispositivo.getMarca());
    		
    		if((dispositivo.getConfiguracion().getNombre() == ""))
    		{
    			statementInsertar.setNull(6, Types.VARCHAR);
    		}
    		else
    		{
        		statementInsertar.setString(6, dispositivo.getConfiguracion().getNombre());
    		}

    		statementInsertar.executeUpdate();
    
    }
    catch (SQLException S) {
    	throw new ConnectionFailureException("Error en la base de datos");
    }
	}

	@Override
	public void update(Dispositivo dispositivo) throws ConnectionFailureException {
         String update = "UPDATE dispositivos SET estado =?, descripcion=?, modelo=?, marca=?, configuracion=? WHERE nombre = ?";
     		 
		 try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave); 
			PreparedStatement statementUpdate = myConexion.prepareStatement(update))
			{		 
				statementUpdate.setBoolean(1, dispositivo.getEstado());
				statementUpdate.setString(2, dispositivo.getDescripcion());
				statementUpdate.setString(3, dispositivo.getModelo());
				statementUpdate.setString(4, dispositivo.getMarca());
				statementUpdate.setString(5, dispositivo.getConfiguracion().getNombre());
				statementUpdate.setString(6, dispositivo.getNombre());
				statementUpdate.executeUpdate();

			
		    }
		    catch (SQLException S) {
	        	throw new ConnectionFailureException("Error en la base de datos");
		    }
	}

	@Override
	public Dispositivo find(String nombre) throws ConnectionFailureException {
	    Dispositivo dispositivoEncontrado = null;
	    String buscar = "SELECT * FROM dispositivos WHERE nombre = ?";
	    try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
	    	PreparedStatement busquedaDispositivo = myConexion.prepareStatement(buscar))
	    	{    		
	    	busquedaDispositivo.setString(1, nombre);	    	
	    	try (ResultSet resultadoBusqueda = busquedaDispositivo.executeQuery())
	    	{
	    	if (resultadoBusqueda.next()) {
	    		dispositivoEncontrado = new Dispositivo();
	    		dispositivoEncontrado.setEstado(resultadoBusqueda.getBoolean("estado"));
	    		dispositivoEncontrado.setNombre(resultadoBusqueda.getString("nombre"));
	    		dispositivoEncontrado.setDescripcion(resultadoBusqueda.getString("descripcion"));
	    		dispositivoEncontrado.setModelo(resultadoBusqueda.getString("modelo"));
	    		dispositivoEncontrado.setMarca(resultadoBusqueda.getString("marca"));
	    		Configuracion configuracion = configDAO.find(resultadoBusqueda.getString("configuracion"));
	    		dispositivoEncontrado.setConfiguracion(configuracion);
	   	    	}
	    	}
	    }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return dispositivoEncontrado;
	}

	@Override
	public void remove(String nombre) throws ConnectionFailureException {
		String deleteConfig = "DELETE FROM dispositivos WHERE nombre = ?";		
		try (Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
			PreparedStatement borrarDispositivo = myConexion.prepareStatement(deleteConfig))
			{
			
			borrarDispositivo.setString(1, nombre);
			borrarDispositivo.executeUpdate();
			
		}
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}


	@Override
	public Set<Dispositivo> findAll() throws ConnectionFailureException
	{
    Set<Dispositivo> listaDispositivo = new HashSet<>();
    String busqueda = "SELECT * FROM dispositivos";		    
    try ( Connection myConexion = DriverManager.getConnection(conexion,usuario,clave);
		PreparedStatement buscarDispositivo = myConexion.prepareStatement(busqueda);
		ResultSet resultBusquedaDispositivos = buscarDispositivo.executeQuery())
    	{
	        while (resultBusquedaDispositivos.next()) {            
	    	    Dispositivo dispositivo = new Dispositivo();
	    	    dispositivo.setEstado(resultBusquedaDispositivos.getBoolean("estado"));
	    	    dispositivo.setNombre(resultBusquedaDispositivos.getString("nombre"));
	    	    dispositivo.setDescripcion(resultBusquedaDispositivos.getString("descripcion"));
	    	    dispositivo.setModelo(resultBusquedaDispositivos.getString("modelo"));
	    	    dispositivo.setMarca(resultBusquedaDispositivos.getString("marca"));
	    		Configuracion configuracion = configDAO.find(resultBusquedaDispositivos.getString("configuracion"));
	    		dispositivo.setConfiguracion(configuracion);
	    		listaDispositivo.add(dispositivo);
    		
	        }	
	    }
    catch (SQLException S) {
    	throw new ConnectionFailureException("Error en la base de datos");
    }
    return listaDispositivo;
    }
}