package ar.edu.unrn.seminario.accesos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import ar.edu.unrn.seminario.api.ParametroDAO;
import ar.edu.unrn.seminario.modelo.Parametro;
import ar.edu.unrn.seminario.modelo.UnidadMedida;
import ar.edu.unrn.seminario.api.UnidadMedidaDAO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.ReferenceDataException;


public class ParametroDAOJDBC implements ParametroDAO{
	private final static String conexion = "jdbc:mysql://localhost:3306/gepdm_2grupo";
	private final static String usuario= "Tonhy";
	private final static String clave = "@i)[Fk8Z8p-Ho5Rb";
	private  UnidadMedidaDAO unidadDAO= new UnidadMedidaDAOJDBC();

    @Override
    public void create(Parametro parametro) throws ConnectionFailureException{
        String consultaSQL = "INSERT INTO parametros (nombre,descripcion,unidadPrincipal) VALUES (?, ?, ?)";
        String consultaTabla = "INSERT INTO parametro_unidad (nombreParametro, nombreUnidad) VALUES (?, ?)";
        Set <UnidadMedida> listaUnidades= parametro.getUnidadMedida();

        try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
        	PreparedStatement statementCrear = myConexion.prepareStatement(consultaSQL);
        	PreparedStatement statementTablaIntermedia = myConexion.prepareStatement(consultaTabla))
        {
            statementCrear.setString(1, parametro.getNombre());
            statementCrear.setString(2, parametro.getDescripcion());
            statementCrear.setString(3, parametro.getUnidadPrincipal().getNombre());  
            statementCrear.executeUpdate();
            for (UnidadMedida u : listaUnidades)
            {
            	statementTablaIntermedia.setString(1, parametro.getNombre());
            	statementTablaIntermedia.setString(2, u.getNombre());
            	statementTablaIntermedia.executeUpdate();
            }
        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
    }

	@Override
	public void update(Parametro parametro) throws ConnectionFailureException
	{
	     Set <UnidadMedida> listaUnidades= parametro.getUnidadMedida();
         String delete = "DELETE FROM  parametro_unidad WHERE nombreParametro = ?";
         String update = "UPDATE parametros SET descripcion = ?, unidadPrincipal = ? WHERE nombre = ?";
         String tablaIntermedia = "INSERT INTO parametro_unidad (nombreParametro, nombreUnidad) VALUES (?, ?)";

        try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
        	PreparedStatement statementDelete = myConexion.prepareStatement(delete);
        	PreparedStatement statementUpdate = myConexion.prepareStatement(update);
        	PreparedStatement statementTablaIntermedia = myConexion.prepareStatement(tablaIntermedia))
        {
        	statementDelete.setString(1, parametro.getNombre());
            statementDelete.executeUpdate();
            
            statementUpdate.setString(1, parametro.getDescripcion());
            statementUpdate.setString(2, parametro.getUnidadPrincipal().getNombre());
            statementUpdate.setString(3, parametro.getNombre());
            statementUpdate.executeUpdate();
            
            for (UnidadMedida u : listaUnidades)
            {
            	statementTablaIntermedia.setString(1, parametro.getNombre());
            	statementTablaIntermedia.setString(2, u.getNombre());
            	statementTablaIntermedia.executeUpdate();
            }
        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}

	@Override
	public void remove(String nombre) throws ConnectionFailureException
	{
        String deleteParametro = "DELETE FROM parametros WHERE nombre = ?";
        String deleteTabla = "DELETE FROM parametro_unidad WHERE nombreParametro = ?";
        
        try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
        	PreparedStatement statementTabla = myConexion.prepareStatement(deleteTabla);
        	PreparedStatement statementParametro = myConexion.prepareStatement(deleteParametro))
        {
        	statementTabla.setString(1, nombre);
        	statementTabla.executeUpdate();
        	
        	statementParametro.setString(1, nombre);
        	statementParametro.executeUpdate();
        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	}
	
	@Override
	public Parametro find(String nombre) throws ConnectionFailureException
	{
	    Parametro parametroEncontrado = null;
	    UnidadMedida unidadMedida = new UnidadMedida();
	    Set<UnidadMedida> listaUnidades = new HashSet<>();
	    String parametroObtener = "SELECT * FROM parametros WHERE nombre = ?";
	    String obtenerUnidades = "SELECT * FROM parametro_unidad pu JOIN unidadesmedida lu ON pu.nombreUnidad = lu.nombre WHERE pu.nombreParametro = ?";
        try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
        	PreparedStatement statementParametro = myConexion.prepareStatement(parametroObtener);
        	PreparedStatement statemenUnidades = myConexion.prepareStatement(obtenerUnidades);
        	)
        {
        	statementParametro.setString(1, nombre);
        	statemenUnidades.setString(1, nombre);
        	try(ResultSet resultSetParametro = statementParametro.executeQuery();
        	ResultSet resultSetUnidades = statemenUnidades.executeQuery())
        	{
                if (resultSetParametro.next())
                {
                    parametroEncontrado = new Parametro();
                    parametroEncontrado.setNombre(resultSetParametro.getString("nombre"));
                    parametroEncontrado.setDescripcion(resultSetParametro.getString("descripcion"));
                    
                    while(resultSetUnidades.next())
                    {
                    	unidadMedida.setNombre(resultSetUnidades.getString("nombre"));
                    	unidadMedida.setTipoDato(resultSetUnidades.getString("tipoDato"));
                    	unidadMedida.setSimbolo(resultSetUnidades.getString("simbolo"));
                    	listaUnidades.add(unidadMedida);
                    }
                    UnidadMedida unidadPrincipal = unidadDAO.find(resultSetParametro.getString("unidadPrincipal"));
                    parametroEncontrado.setUnidadPrincipal(unidadPrincipal);
                    
                    parametroEncontrado.setUnidadMedida(listaUnidades);
             	}  
        	}
        }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return parametroEncontrado;
	}

	@Override
	public Set<Parametro> findAll() throws ConnectionFailureException
	{
	    Set<Parametro> listaParametros = new HashSet<>();
	    String obtenerParametros = "SELECT * FROM parametros";
	    String obtenerUnidades = "SELECT * FROM parametro_unidad pu JOIN unidadesmedida lu ON pu.nombreUnidad = lu.nombre WHERE pu.nombreParametro = ?";

	    try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
	    	PreparedStatement statement = myConexion.prepareStatement(obtenerParametros);
	    	PreparedStatement statemenUnidades = myConexion.prepareStatement(obtenerUnidades);
	        ResultSet todosLosParametros = statement.executeQuery();)
	    {
	        while (todosLosParametros.next())
	        {
	            Parametro parametro = new Parametro();
	            parametro.setNombre(todosLosParametros.getString("nombre"));
	            parametro.setDescripcion(todosLosParametros.getString("descripcion"));
	            UnidadMedida unidadPrincipal = unidadDAO.find(todosLosParametros.getString("unidadPrincipal"));
                parametro.setUnidadPrincipal(unidadPrincipal);
                
                statemenUnidades.setString(1, parametro.getNombre());

        	    Set<UnidadMedida> listaUnidades = new HashSet<>();
        	    
                try(ResultSet resultSetUnidades = statemenUnidades.executeQuery())
                {
                	while(resultSetUnidades.next())
                	{
                		UnidadMedida unidad = new UnidadMedida();
                		unidad.setNombre(resultSetUnidades.getString("nombre"));
                		unidad.setTipoDato(resultSetUnidades.getString("tipoDato"));
                		unidad.setSimbolo(resultSetUnidades.getString("simbolo"));
                		
                		listaUnidades.add(unidad);
                	}
                }               

            	parametro.setUnidadMedida(listaUnidades);
                
                listaParametros.add(parametro);
	        }
	    }
	    catch (SQLException S) {
        	throw new ConnectionFailureException("Error en la base de datos");
	    }
	    return listaParametros;
	}
}