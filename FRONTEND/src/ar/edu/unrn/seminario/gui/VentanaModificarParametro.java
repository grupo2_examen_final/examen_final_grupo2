package ar.edu.unrn.seminario.gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.dto.UnidadMedidaDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;

import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JTable;

public class VentanaModificarParametro extends JFrame implements ActionListener, ListSelectionListener{

	private JPanel contentPane;
	private IApi iApi;
	private JTextArea textDescripcion;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JTable table;
	private DefaultTableModel modeloPrincipal;
	private DefaultTableModel modeloUnidades;
	private JTable jTablePrincipal;
	private String[] titulosPrincipal = {"Nombre", "Tipo", "Simbolo"};
	private String[] tituloUnidades = {"Unidades del Parametro"};
	private JScrollPane jScrollPanePrincipal;
	private JTable jTableUnidades;
	private JScrollPane jScrollPaneUnidades;
	private String parametroModificar;
	private JTextField textParametro;
	private JLabel lblUnidadPrincipal;
	private JComboBox cBoxUnidadPrincipal;
	private DefaultComboBoxModel modeloCBox;
	private JLabel lblUnidadActual;
	private JTextField textUnidadActual;
    private JScrollPane jScrollPaneTextArea;

	public VentanaModificarParametro(IApi memoryApi, Object parametroModificar) throws ConnectionFailureException
	{
		this.iApi = memoryApi;
		this.parametroModificar = (String) parametroModificar;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 723, 414);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(394, 80, 110, 29);
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblDescripcion);
		
		JLabel lblUnidadDeMedida = new JLabel("Unidad de Medida");
		lblUnidadDeMedida.setBounds(10, 170, 169, 29);
		lblUnidadDeMedida.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblUnidadDeMedida);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(199, 343, 98, 29);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(343, 343, 98, 29);
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPane.add(btnCancelar);
		
		textDescripcion = new JTextArea();
		
        textDescripcion.setLineWrap(true);
        textDescripcion.setWrapStyleWord(true);
        
        jScrollPaneTextArea = new JScrollPane(textDescripcion);
		jScrollPaneTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPaneTextArea.setBounds(514, 80, 171, 87);
		
		contentPane.add(jScrollPaneTextArea);
		
		JLabel lblParametro = new JLabel("Parametro");
		lblParametro.setBounds(10, 28, 98, 29);
		lblParametro.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblParametro);
		
		table = new JTable();
		table.setBounds(0, 0, 0, 0);
		contentPane.add(table);
		
		modeloPrincipal = new DefaultTableModel(titulosPrincipal, 0);
		modeloUnidades = new DefaultTableModel(tituloUnidades, 0);
		
		jTablePrincipal = new JTable(modeloPrincipal);
		jTablePrincipal.setBounds(1, 25, 308, 0);
		contentPane.add(jTablePrincipal);
		
		jScrollPanePrincipal = new JScrollPane(jTablePrincipal);
		jScrollPanePrincipal.setBounds(199, 175, 310, 148);
		contentPane.add(jScrollPanePrincipal);
		
		jTableUnidades = new JTable(modeloUnidades);
		jTableUnidades.setBounds(1, 25, 177, 0);
		
		contentPane.add(jTableUnidades);
		
		jScrollPaneUnidades = new JScrollPane(jTableUnidades);
		jScrollPaneUnidades.setBounds(506, 175, 179, 148);
		contentPane.add(jScrollPaneUnidades);
	
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);
		jTableUnidades.setEnabled(false);
		
		textParametro = new JTextField();
		textParametro.setBounds(210, 28, 159, 29);
		textParametro.setEditable(false);
		textParametro.setColumns(10);
		textParametro.setText(this.parametroModificar);
		contentPane.add(textParametro);
		
		lblUnidadPrincipal = new JLabel("Unidad Principal");
		lblUnidadPrincipal.setBounds(10, 249, 169, 29);
		lblUnidadPrincipal.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblUnidadPrincipal);
		
		modeloCBox = new DefaultComboBoxModel<>();
		
		cBoxUnidadPrincipal = new JComboBox();
		cBoxUnidadPrincipal.setBounds(10, 288, 169, 21);
		cBoxUnidadPrincipal.setModel(modeloCBox);
		contentPane.add(cBoxUnidadPrincipal);
		
		jTablePrincipal.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		lblUnidadActual = new JLabel("U. Principal Actual");
		lblUnidadActual.setBounds(10, 80, 169, 29);
		lblUnidadActual.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lblUnidadActual);
		
		textUnidadActual = new JTextField();
		textUnidadActual.setBounds(210, 84, 159, 29);
		textUnidadActual.setEditable(false);
		textUnidadActual.setColumns(10);
		contentPane.add(textUnidadActual);
		
		jTablePrincipal.getSelectionModel().addListSelectionListener(this);
		
		inicializarTabla();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{	
		if(e.getSource() == btnAceptar)
		{
			try
			{
				if(jTablePrincipal.getSelectedRowCount() >= 1)
				{
					//el bucle for obtiene las celdas seleccionadas de la tabla y las guarda en una lista de tipo String
					int[] filasSeleccionadas = jTablePrincipal.getSelectedRows();
			        List<String> unidadesSeleccionadas = new ArrayList<>();
					for (int fila : filasSeleccionadas)
					{
			            String nombre = (String) jTablePrincipal.getValueAt(fila, 0);
			            unidadesSeleccionadas.add(nombre);
			        }
					//se crea un objeto de tipo parametroDTO al cual se le asigna el parametro seleccionado en el combo box
					//luego, se llama al metodo modificarParametro, el cual se encarga de modificar los parametros
					ParametroDTO parametroModificar = iApi.buscarParametroDTO((this.parametroModificar));
					
					iApi.modificarParametro(parametroModificar.getNombre(), unidadesSeleccionadas, textDescripcion.getText(), (String) cBoxUnidadPrincipal.getSelectedItem());
				}
				
				else
				{
					ParametroDTO parametroModificar = iApi.buscarParametroDTO((this.parametroModificar));					
					iApi.modificarParametro(parametroModificar.getNombre(), parametroModificar.getUnidadMedida(), textDescripcion.getText(), parametroModificar.getUnidadPrincipal());
				}
				
				JOptionPane.showMessageDialog(null, "El parametro se ha modificado correctamente");
				this.dispose();
				VentanaParametros ventanaNueva = new VentanaParametros(iApi);
				ventanaNueva.setVisible(true);
				ventanaNueva.setLocationRelativeTo(null);
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
			catch(DataEmptyException l)
			{
				JOptionPane.showMessageDialog(null, "ATENCION: " + l.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				
			}
		}
		
		else if(e.getSource() == btnCancelar)
		{
			try
			{
				VentanaParametros ventanaNueva = new VentanaParametros(iApi);
				ventanaNueva.setVisible(true);
				ventanaNueva.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
	}
	
	private void llenarTabla(List<UnidadMedidaDTO> listaUnidadMedida) throws ConnectionFailureException
	{
		ParametroDTO parametroSeleccionado = iApi.buscarParametroDTO((this.parametroModificar));
		List<String> unidadesParametro = parametroSeleccionado.getUnidadMedida();
		
		for (String string : unidadesParametro)
		{
			Object[] fila = new Object[1];
			fila[0] = string;
			
			modeloUnidades.addRow(fila);
		}
		
		for (UnidadMedidaDTO unidadMedidaDTO : listaUnidadMedida)
		{
			Object[] fila = new Object[3];
			fila[0] = unidadMedidaDTO.getNombre();
			fila[1] = unidadMedidaDTO.getTipoDato();
			fila[2] = unidadMedidaDTO.getSimbolo();
			
			modeloPrincipal.addRow(fila);
		}
	}
	
	private void limpiarTabla()
	{
		modeloPrincipal.setRowCount(0);
		modeloPrincipal.fireTableDataChanged();
		modeloUnidades.setRowCount(0);
		modeloUnidades.fireTableDataChanged();
	}
	
	private void inicializarTabla() throws ConnectionFailureException
	{
		limpiarTabla();
		textDescripcion.setText(iApi.buscarParametroDTO(this.parametroModificar).getDescripcion());
		textUnidadActual.setText(iApi.buscarParametroDTO(this.parametroModificar).getUnidadPrincipal());
		llenarTabla(iApi.getUnidadMedidaDTO());
	}

	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		modeloCBox.removeAllElements();
		int[] filasSeleccionadas= jTablePrincipal.getSelectedRows();
		
		for (int fila : filasSeleccionadas)
		{
			modeloCBox.addElement(jTablePrincipal.getValueAt(fila, 0));
		}
	}
}








