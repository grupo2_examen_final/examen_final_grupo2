package ar.edu.unrn.seminario.gui;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.InvalidStateChangeException;
import ar.edu.unrn.seminario.exception.ObjectAlreadyExistException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;
import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class VentanaAgregarDispositivo extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField textBoxNombre;
	private JTextField textBoxMarca;
	private ButtonGroup bg = new ButtonGroup();
	private JTextField textBoxModelo;
	private JRadioButton rdbtnActivado;
	private JRadioButton rdbtnDesactivado;
	private JTextArea textBoxDescripcion;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JComboBox <String>comboBoxConfiguraciones;
	private IApi iApi;
    private JScrollPane jScrollPaneTextArea;
	
	/**
	 * Create the frame.
	 * @throws ConnectionFailureException 
	 */
	public VentanaAgregarDispositivo(IApi iApi) throws ConnectionFailureException {
		this.iApi = iApi;
		
		setTitle("Alta dispositivo");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 460, 397);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textBoxNombre = new JTextField();
		textBoxNombre.setBounds(229, 35, 167, 20);
		contentPane.add(textBoxNombre);
		textBoxNombre.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(47, 35, 96, 20);
		contentPane.add(lblNewLabel);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDescripcion.setBounds(47, 226, 104, 20);
		contentPane.add(lblDescripcion);
		
		textBoxMarca = new JTextField();
		textBoxMarca.setColumns(10);
		textBoxMarca.setBounds(229, 66, 167, 20);
		contentPane.add(textBoxMarca);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEstado.setBounds(47, 136, 86, 23);
		contentPane.add(lblEstado);
		
		textBoxDescripcion = new JTextArea();
		
        textBoxDescripcion.setLineWrap(true);
        textBoxDescripcion.setWrapStyleWord(true);
        
        jScrollPaneTextArea = new JScrollPane(textBoxDescripcion);
		jScrollPaneTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPaneTextArea.setBounds(47, 256, 348, 61);
		
		contentPane.add(jScrollPaneTextArea);
		
		rdbtnDesactivado = new JRadioButton("Desactivado");
		rdbtnDesactivado.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtnDesactivado.setBounds(229, 152, 109, 23);
		contentPane.add(rdbtnDesactivado);
		
		rdbtnActivado = new JRadioButton("Activado");
		rdbtnActivado.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtnActivado.addActionListener(this);
		rdbtnActivado.setBounds(229, 124, 120, 23);
		contentPane.add(rdbtnActivado);
		
		bg.add(rdbtnActivado);
		bg.add(rdbtnDesactivado);
		rdbtnDesactivado.setSelected(true);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setBounds(208, 324, 89, 23);
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this);
		btnCancelar.setBounds(307, 324, 89, 23);
		contentPane.add(btnCancelar);
		
		textBoxModelo = new JTextField();
		textBoxModelo.setColumns(10);
		textBoxModelo.setBounds(229, 97, 167, 20);
		contentPane.add(textBoxModelo);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblModelo.setBounds(47, 100, 86, 14);
		contentPane.add(lblModelo);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMarca.setBounds(47, 68, 86, 17);
		contentPane.add(lblMarca);
		
		comboBoxConfiguraciones = new JComboBox<String>();
		comboBoxConfiguraciones.setBounds(228, 221, 168, 22);
		contentPane.add(comboBoxConfiguraciones);
		
		
		JLabel lblAgregarConfiguracion = new JLabel("Agregar Configuracion");
		lblAgregarConfiguracion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAgregarConfiguracion.setBounds(221, 194, 175, 17);
		contentPane.add(lblAgregarConfiguracion);
		
		llenarCombo();
		
	}
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == btnAceptar)
		{
			try {
				if(rdbtnActivado.isSelected()){
					isComplet(textBoxNombre.getText(), "nombre");
					isComplet(textBoxModelo.getText(), "modelo");
					isComplet(textBoxMarca.getText(), "marca");
				}
				
				String nombre = (String)comboBoxConfiguraciones.getSelectedItem(); 
				iApi.altaDispositivo(textBoxNombre.getText(), textBoxDescripcion.getText() , textBoxModelo.getText(), textBoxMarca.getText(), rdbtnActivado.isSelected(),nombre);
				textBoxNombre.setText("");
				textBoxMarca.setText("");
				textBoxModelo.setText("");
				textBoxDescripcion.setText("");
				rdbtnDesactivado.setSelected(true);
				
				JOptionPane.showMessageDialog(null, "El dispositivo se creo correctamente");
			}
			catch(DataEmptyException dee)
			{
				String mensaje = dee.getMessage();
				JOptionPane.showMessageDialog(null, mensaje, "ERROR", JOptionPane.ERROR_MESSAGE);
			}
			catch(RepeatedNameException oaee)
			{
				JOptionPane.showMessageDialog(null, oaee.getMessage());
			}
			catch(InvalidStateChangeException y)
			{
				JOptionPane.showMessageDialog(null, y.getMessage());
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		else if (e.getSource() == btnCancelar) {
			try
			{
				VentanaDispositivo ventanaDispositivo = new VentanaDispositivo(iApi);
				ventanaDispositivo.setVisible(true);
				ventanaDispositivo.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		else if (e.getSource()== rdbtnActivado) {
			try {
				isComplet(textBoxNombre.getText(), "nombre");
				isComplet(textBoxModelo.getText(), "modelo");
				isComplet(textBoxMarca.getText(), "marca");
			}catch(DataEmptyException exception){
				rdbtnActivado.setSelected(false);
				rdbtnDesactivado.setSelected(true);
				String mensaje = exception.getMessage();
				JOptionPane.showMessageDialog(null, mensaje, "ERROR", JOptionPane.ERROR_MESSAGE);
			}
		}
		}
		
	
	private void isComplet(String campo, String nombre) throws DataEmptyException{
		if(campo.trim().isEmpty()) {
			throw new DataEmptyException("falta el campo " + nombre);
		}
	}
	
	private void llenarCombo() throws ConnectionFailureException {
		List <ConfiguracionDTO> lista= iApi.getConfiguracionDTO();
		
		comboBoxConfiguraciones.addItem("");
		
		for (ConfiguracionDTO config : lista) {
			comboBoxConfiguraciones.addItem(config.getNombre());
		}
	}

}