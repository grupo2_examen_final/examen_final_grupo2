package ar.edu.unrn.seminario.gui;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;
import javax.swing.JComboBox;
import javax.swing.JTextArea;


public class VentanaModificarConfiguracion extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JLabel lblNombre;
	private JTextField textFieldNombreEditado;
	private JTextArea textAreaDescripcion;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JButton btnEliminar;
	private JButton btnAgregar;
	private JComboBox <String> comboBoxListaParametros;
	private String nombreConfig;
	private IApi iApi;
	private JComboBox<String> comboBoxParametrosDeConfiguracion;
	private JFrame ventanaAnterior;
    private JScrollPane jScrollPaneTextArea;

	
	public VentanaModificarConfiguracion(IApi iApi,String nombre, JFrame anterior) throws ConnectionFailureException {
		this.ventanaAnterior=anterior;
		setTitle("Modificar Configuracion");
		this.iApi=iApi;
		this.nombreConfig=nombre;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 436);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombre = new JLabel("NOMBRE");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombre.setBounds(48, 85, 122, 14);
		contentPane.add(lblNombre);
		
		textFieldNombreEditado = new JTextField();
		textFieldNombreEditado.setEditable(false);
		textFieldNombreEditado.setColumns(10);
		textFieldNombreEditado.setBounds(180, 85, 188, 20);
		contentPane.add(textFieldNombreEditado);
		
		comboBoxListaParametros = new JComboBox<String>();
		comboBoxListaParametros.setBounds(180, 277, 188, 22);
		contentPane.add(comboBoxListaParametros);
		
		btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(168, 363, 95, 23);
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("ATRAS");
		btnCancelar.setBounds(279, 363, 95, 23);
		contentPane.add(btnCancelar);
		
		JLabel lblDescripcion = new JLabel("DESCRIPCION");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDescripcion.setBounds(48, 133, 122, 14);
		contentPane.add(lblDescripcion);
				
		textAreaDescripcion = new JTextArea();
		textAreaDescripcion.setEditable(true);
		
        textAreaDescripcion.setLineWrap(true);
        textAreaDescripcion.setWrapStyleWord(true);
        
        jScrollPaneTextArea = new JScrollPane(textAreaDescripcion);
		jScrollPaneTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPaneTextArea.setBounds(180, 131, 188, 54);
		
		contentPane.add(jScrollPaneTextArea);
		
		JLabel lblNewLabelEditar = new JLabel("* Edite los campos que necesite");
		lblNewLabelEditar.setEnabled(false);
		lblNewLabelEditar.setBounds(48, 32, 179, 14);
		contentPane.add(lblNewLabelEditar);
		
		JLabel lblNewLabelDescripcion = new JLabel("Eliminar Parametro");
		lblNewLabelDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabelDescripcion.setBounds(48, 195, 125, 19);
		contentPane.add(lblNewLabelDescripcion);
		
		
		btnEliminar = new JButton("ELIMINAR");
		btnEliminar.setBounds(279, 228, 89, 23);
		btnEliminar.addActionListener(this);
		contentPane.add(btnEliminar);
		
		JLabel lblAgregarParametro = new JLabel("Agregar Parametro");
		lblAgregarParametro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAgregarParametro.setBounds(48, 277, 162, 19);
		contentPane.add(lblAgregarParametro);
		

		
		btnAgregar = new JButton("AGREGAR");
		btnAgregar.setBounds(279, 310, 89, 23);
		btnAgregar.addActionListener(this);
		contentPane.add(btnAgregar);
		
		comboBoxParametrosDeConfiguracion = new JComboBox<String>();
		comboBoxParametrosDeConfiguracion.setBounds(180, 195, 188, 22);
		contentPane.add(comboBoxParametrosDeConfiguracion);
		
		
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);

		inicializar();
		llenarParametrosDeConfiguraicion(nombre);
		llenarParametrosParaAgregar();
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == btnAceptar)
			{
			try {
				String descripcion= this.textAreaDescripcion.getText();			
				List <String> listaParametros = new ArrayList <>();
				for (int i = 0; i < comboBoxParametrosDeConfiguracion.getItemCount(); i++) {
				     listaParametros.add((String) comboBoxParametrosDeConfiguracion.getItemAt(i));
				}
				iApi.modificarConfiguracion(nombreConfig, descripcion, listaParametros);
           		JOptionPane.showMessageDialog(null, "Se modifico correctamente: " );	
				VentanaConfiguracion ventanaAnterior = new VentanaConfiguracion(iApi);
				ventanaAnterior.setLocationRelativeTo(null);
				ventanaAnterior.setVisible(true);
				this.dispose();
			}
           	catch (DataEmptyException j) {
           		JOptionPane.showMessageDialog(null, j.getMessage());	
           		}
			catch (ConnectionFailureException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
           }
		
		
		else if(e.getSource() == btnCancelar)
		{
				try
				{
					VentanaConfiguracion ventanaAnterior = new VentanaConfiguracion(iApi);
					ventanaAnterior.setLocationRelativeTo(null);
					ventanaAnterior.setVisible(true);
					this.dispose();
				}
				catch(ConnectionFailureException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
		}
		else if (e.getSource() == btnAgregar){
			String parametr0 = (String) this.comboBoxListaParametros.getSelectedItem();
			try {
			iApi.agregarParametroaConfiguracion(nombreConfig, parametr0);
			llenarParametrosDeConfiguraicion(nombreConfig);			
			}catch (ConnectionFailureException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}catch (RepeatedNameException l) {
				JOptionPane.showMessageDialog(null, "Error: " + l.getMessage());
			}
		}
		else if (e.getSource() == btnEliminar){
			String parametr0 = (String) this.comboBoxParametrosDeConfiguracion.getSelectedItem();
			try
			{
				iApi.quitarParametroaConfiguracion(nombreConfig, parametr0);
				llenarParametrosDeConfiguraicion(nombreConfig);
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}

		}
	}
	private void llenarParametrosDeConfiguraicion(String nombreConfig) throws ConnectionFailureException {
			comboBoxParametrosDeConfiguracion.removeAllItems();
			for (String dto : iApi.buscarConfiguracionDTO(nombreConfig).getListaParametros()) {
				comboBoxParametrosDeConfiguracion.addItem(dto);
			}
		}
	private void llenarParametrosParaAgregar() throws ConnectionFailureException {
			for (ParametroDTO dto : iApi.getParametroDTO()) {
				comboBoxListaParametros.addItem(dto.getNombre());
			}
			
			
}
	private void inicializar() throws ConnectionFailureException {
			this.textFieldNombreEditado.setText(iApi.buscarConfiguracionDTO(nombreConfig).getNombre());
			this.textAreaDescripcion.setText(iApi.buscarConfiguracionDTO(nombreConfig).getDescripcion());
		}
}