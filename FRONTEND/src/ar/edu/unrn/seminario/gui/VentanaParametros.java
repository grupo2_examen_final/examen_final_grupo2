package ar.edu.unrn.seminario.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import java.awt.BorderLayout;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.MemoryApi;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.dto.UnidadMedidaDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.ReferenceDataException;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class VentanaParametros extends JFrame implements ActionListener, MouseMotionListener, ListSelectionListener{

	private JPanel contentPane;
	private JTable jTable;
	private DefaultTableModel modelo;
	private String[] titulos = {"Nombre", "Unidad", "Simbolo", "Tipo", "Descripcion"};
	private IApi iApi;
	private JButton btnBuscar;
	private JTextField textBuscar;
	private JButton btnRestablecer;
	private JPanel panel;
	private JButton btnEliminar;
	private JButton btnAgregar;
	private JButton btnModificar;
	private JButton btnSalir;
	private JButton btnInvisible;
	private JMenuBar menuBar;
	

	
	public VentanaParametros(IApi iApi) throws ConnectionFailureException{
		this.iApi = iApi;
		jTable = new JTable();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 697, 425);
		
		modelo = new DefaultTableModel(titulos, 0);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		btnSalir = new JButton("Salir");
		menuBar.add(btnSalir);
		btnSalir.addActionListener(this);
		
		btnInvisible = new JButton("                            ");
		menuBar.add(btnInvisible);
		btnInvisible.setEnabled(false);
		btnInvisible.setBorderPainted(false);
		
		btnBuscar = new JButton("Buscar");
		menuBar.add(btnBuscar);
		
		btnBuscar.addActionListener(this);
		
		textBuscar = new JTextField();
		menuBar.add(textBuscar);
		textBuscar.setColumns(10);
		
		btnRestablecer = new JButton("Restablecer");
		menuBar.add(btnRestablecer);
		btnRestablecer.addActionListener(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		getContentPane().add(new JScrollPane(jTable), BorderLayout.CENTER);
		
		setLocationRelativeTo(null);
		jTable.setBounds(100, 100, 697, 425);
		jTable.setModel(modelo);
		jTable.add(new JScrollPane());
		jTable.setRowSelectionAllowed(true);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		btnAgregar = new JButton("Agregar");
		panel.add(btnAgregar);
		btnAgregar.addActionListener(this);
		
		btnModificar = new JButton("Modificar");
		panel.add(btnModificar);
		btnModificar.addActionListener(this);
		
		btnEliminar = new JButton("Eliminar");
		panel.add(btnEliminar);
		btnEliminar.addActionListener(this);
		
		jTable.addMouseMotionListener(this);
		jTable.getSelectionModel().addListSelectionListener(this);
		
		btnEliminar.setEnabled(false);
		btnModificar.setEnabled(false);
		
		inicializarTabla();
	}
	
	private void llenarTabla(List<ParametroDTO> listaParametro) throws ConnectionFailureException
	{
		for (ParametroDTO parametroDTO : listaParametro)
		{
			Object[] fila = new Object[5];
			fila[0] = parametroDTO.getNombre();
			fila[1] = parametroDTO.getUnidadPrincipal();
			fila[2] = iApi.buscarUnidadMedidaDTO(parametroDTO.getUnidadPrincipal()).getSimbolo();
			fila[3] = iApi.buscarUnidadMedidaDTO(parametroDTO.getUnidadPrincipal()).getTipoDato();
			fila[4] = parametroDTO.getDescripcion();
			
			modelo.addRow(fila);
		}
	}
	
	private void limpiarTabla()
	{
		modelo.setRowCount(0);
		modelo.fireTableDataChanged();
	}
	
	private void inicializarTabla() throws ConnectionFailureException
	{
		limpiarTabla();
		llenarTabla(iApi.getParametroDTO());
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == btnAgregar)
		{
			try {
				if(!iApi.getUnidadMedidaDTO().isEmpty())
				{
					VentanaAgregarParametro ventanaAgregar = new VentanaAgregarParametro(iApi);
					ventanaAgregar.setVisible(true);
					ventanaAgregar.setLocationRelativeTo(null);
					this.dispose();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "No hay unidades de medida disponibles. Primero inicializa alguna unidad");
				}
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		
		else if(e.getSource() == btnModificar)
		{
			try
			{
				VentanaModificarParametro ventanaModificar = new VentanaModificarParametro(iApi, jTable.getValueAt(jTable.getSelectedRow(), 0));
				ventanaModificar.setVisible(true);
				ventanaModificar.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		
		else if(e.getSource() == btnEliminar)
		{
			int respuesta = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el parametro?", "Confirmacion", JOptionPane.YES_NO_OPTION);
			if(respuesta == JOptionPane.YES_OPTION)
			{
				int fila = jTable.getSelectedRow();
				try
				{
					iApi.eliminarParametro((String) jTable.getValueAt(fila, 0));
					limpiarTabla();
					llenarTabla(iApi.getParametroDTO());
					JOptionPane.showMessageDialog(null, "El parametro se elimino correctamente");
				}
				catch (ConnectionFailureException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
				catch (ReferenceDataException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}

			}
			else 
			{
				JOptionPane.showMessageDialog(null, "Se ha cancelado la eliminacion");
			}
		}
		
		else if(e.getSource() == btnBuscar)
		{
			limpiarTabla();
			try
			{
				llenarTabla(iApi.filtrarParametroDTO(textBuscar.getText(), iApi.getParametroDTO()));
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		
		else if(e.getSource() == btnRestablecer)
		{
			limpiarTabla();
			try
			{
				llenarTabla(iApi.getParametroDTO());
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		else if(e.getSource() == btnSalir)
		{
			VPrincipalGrupo2 ventanaGrupo= new VPrincipalGrupo2 (this.iApi);
			ventanaGrupo.setVisible(true);
			ventanaGrupo.setLocationRelativeTo(null);
			this.dispose();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		if(e.getComponent() == jTable)
		{
			int fila = jTable.rowAtPoint(e.getPoint());
			int columna = jTable.columnAtPoint(e.getPoint());
			
			ParametroDTO parametro = new ParametroDTO();
			try
			{
				parametro = iApi.getParametroDTO().get(fila);
			}
			catch (ConnectionFailureException e1)
			{
				
			}
			if(fila >= 0 && columna == 1)
			{
				jTable.setToolTipText("El parametro posee las siguientes unidades de medida " + parametro.getUnidadMedida());
			}
		
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		if(jTable.getSelectedRowCount() == 1)
		{
			btnEliminar.setEnabled(true);
			btnModificar.setEnabled(true);
		}
		
		else if(jTable.getSelectedRowCount()>1)
		{
			btnEliminar.setEnabled(false);
			btnModificar.setEnabled(false);
		}
	}
}