package ar.edu.unrn.seminario.gui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.InvalidStateChangeException;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowEvent;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JTextField;

public class VentanaDispositivo extends JFrame implements ActionListener, ListSelectionListener{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
    private JButton btnActivarDispositivo;
    private JButton btnDesactivarDispositivo;
    private JButton btnCancelar;
    private JButton btnModificarDispositivo;
	private DefaultTableModel modelo;
	private IApi iApi;
	private JButton btnAgregar;
	private JButton btnEliminar;
	private JTable jTable;
	private JMenuBar menuBar;
	private JButton btnBuscar;
	private JTextField textBuscar;
	private JPanel panelBotones;

	/**
	 * Create the frame.
	 * @throws ConnectionFailureException 
	 */
	public VentanaDispositivo(IApi iApi) throws ConnectionFailureException {
		this.iApi = iApi;
		
		jTable = new JTable();
		setTitle("Listado de dispositivos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 901, 447);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setLocationRelativeTo(null);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
			
		btnCancelar = new JButton("Salir");
		menuBar.add(btnCancelar);
		
		textBuscar = new JTextField();
		menuBar.add(textBuscar);
		textBuscar.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		menuBar.add(btnBuscar);
		btnCancelar.addActionListener(this);
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		

		getContentPane().add(new JScrollPane(jTable), BorderLayout.CENTER);
		
		
		panelBotones = new JPanel();
		contentPane.add(panelBotones, BorderLayout.SOUTH);
		
		btnAgregar = new JButton("Agregar");
		panelBotones.add(btnAgregar);
		btnAgregar.addActionListener(this);
		
		
		
		btnActivarDispositivo = new JButton("Activar");
		panelBotones.add(btnActivarDispositivo);
		btnActivarDispositivo.setEnabled(false);
		
		btnActivarDispositivo.addActionListener(this);
		
		
		
		btnDesactivarDispositivo = new JButton("Desactivar");
		panelBotones.add(btnDesactivarDispositivo);
		btnDesactivarDispositivo.setEnabled(false);
		btnDesactivarDispositivo.addActionListener(this);
		
		
		
		btnModificarDispositivo = new JButton("Modificar");
		panelBotones.add(btnModificarDispositivo);
		btnModificarDispositivo.setEnabled(false);
		
		btnEliminar = new JButton("Eliminar");
		panelBotones.add(btnEliminar);
		btnEliminar.setEnabled(false);
		btnEliminar.addActionListener(this);
		btnModificarDispositivo.addActionListener(this);
		
		btnBuscar.addActionListener(this);
		
		String[] titulos = { "NOMBRE", "Modelo", "Marca","Descripcion", "Estado","Parametros"};
		modelo = new DefaultTableModel(new Object[][] {}, titulos) {
		
			@Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
		};
		
		jTable.setModel(modelo);
		jTable.add(new JScrollPane());
		
		jTable.getTableHeader().setReorderingAllowed(false);
		jTable.getSelectionModel().addListSelectionListener(this);

		llenarTabla(iApi.getDispositivosDTO());
	}

		
	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		int filaSelec = jTable.getSelectedRow();
		if (!e.getValueIsAdjusting()) 
		{
			 if (e.getSource() == jTable.getSelectionModel()) 
			 {
				 if(jTable.getSelectedRowCount() == 1)
				 {
				    if (filaSelec != -1)
				    {
				        String nombreDispositivo = (String) jTable.getModel().getValueAt(filaSelec, 0);
						try
						{
							DispositivoDTO dispositivoDTO = iApi.recuperarDispositivoDTO(nombreDispositivo);
					        btnActivarDispositivo.setEnabled(!dispositivoDTO.isEstado());
					        btnDesactivarDispositivo.setEnabled(dispositivoDTO.isEstado());
					        btnModificarDispositivo.setEnabled(true);
					        btnEliminar.setEnabled(true);
						}
						catch (ConnectionFailureException e1)
						{
							JOptionPane.showMessageDialog(null, e1.getMessage());
							VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
							nuevaVentana.setLocationRelativeTo(null);
							nuevaVentana.setVisible(true);
							this.dispose();
						}

				    }
				 }
			    else
			    {
			        btnActivarDispositivo.setEnabled(false);
			        btnDesactivarDispositivo.setEnabled(false);
			        btnModificarDispositivo.setEnabled(false);
			        btnEliminar.setEnabled(false);				
			    }
			}
		}
	}		
			
	@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == btnEliminar) {
		
				int reply = JOptionPane.showConfirmDialog(null,
				"Estas seguro que desea eliminar el dispositivo?", "Confirma la eliminacion.",
				JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					try
					{
				        String nombreDispositivo = (String) jTable.getModel().getValueAt(jTable.getSelectedRow(), 0);
				        DispositivoDTO dispositivoDTO = iApi.recuperarDispositivoDTO(nombreDispositivo);
						iApi.eliminarDispositivo(dispositivoDTO.getNombre());
						refreshTable();
				        actualizarEstadoBtn();
						JOptionPane.showMessageDialog(null, "El dispositivo se elimino correctamente");
					}
					catch (ConnectionFailureException e1)
					{
						JOptionPane.showMessageDialog(null, e1.getMessage());
						VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
						nuevaVentana.setLocationRelativeTo(null);
						nuevaVentana.setVisible(true);
						this.dispose();
					}
				}
				
				else
				{
					JOptionPane.showMessageDialog(null, "Se ha cancelado la eliminacion");
				}
				
			}
			else if (e.getSource()==btnModificarDispositivo) {
				
				int filaSeleccionada = jTable.getSelectedRow();
		        if (filaSeleccionada != -1) {
		           String nombreDispositivo = (String) jTable.getModel().getValueAt(jTable.getSelectedRow(), 0);
		           DispositivoDTO dispositivoDTO;
					try
					{
						dispositivoDTO = iApi.recuperarDispositivoDTO(nombreDispositivo);
						VentanaModificarDispositivo modDispositivo = new VentanaModificarDispositivo(iApi, dispositivoDTO);
						modDispositivo.setLocationRelativeTo(null);
						modDispositivo.setVisible(true);
						this.dispose();
					}
					catch (ConnectionFailureException e1)
					{
						JOptionPane.showMessageDialog(null, e1.getMessage());
						VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
						nuevaVentana.setLocationRelativeTo(null);
						nuevaVentana.setVisible(true);
						this.dispose();
					}

		        }
		    }
			
			else if (e.getSource()==btnDesactivarDispositivo) {

				try {
					// Obtener el nombre del dispositivo seleccionado en la tabla.
					String nombreDispositivo = (String) jTable.getModel().getValueAt(jTable.getSelectedRow(), 0);
					// Desactivar el dispositivo.
					iApi.desactivarDispositivo(nombreDispositivo);
					// Limpiar la tabla y volver a llenarla.
					refreshTable();
			        actualizarEstadoBtn();
				}
				catch(ArrayIndexOutOfBoundsException exception)
				{
					JOptionPane.showMessageDialog(null, "Debe seleccionar un dispositivo", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
				catch (ConnectionFailureException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
			}
			else if (e.getSource()==btnActivarDispositivo) {

				try
				{
					String nombreDispositivo = (String) jTable.getModel().getValueAt(jTable.getSelectedRow(), 0);
					iApi.activarDispositivo(nombreDispositivo);
					refreshTable();
			        actualizarEstadoBtn();
				}
				catch(ArrayIndexOutOfBoundsException exception)
				{
					JOptionPane.showMessageDialog(null, "Debe seleccionar un dispositivo", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
				catch(InvalidStateChangeException isc)
				{
					JOptionPane.showMessageDialog(null, "El dispositivo necesita tener una configuracion par activarse", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
				catch (ConnectionFailureException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
			}
			else if (e.getSource()==btnAgregar )
			{
				try
				{
					VentanaAgregarDispositivo ventanaDispositivo = new VentanaAgregarDispositivo(iApi);
					ventanaDispositivo.setLocationRelativeTo(null);
					ventanaDispositivo.setVisible(true);
					this.dispose();
				}
				catch(ConnectionFailureException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
			}
			else if (e.getSource()==btnCancelar )
			{
				VPrincipalGrupo2 ventanaPrincipal = new VPrincipalGrupo2(iApi);
				ventanaPrincipal.setLocationRelativeTo(null);
				ventanaPrincipal.setVisible(true);
				this.dispose();
			}
			
			else if(e.getSource() == btnBuscar)
			{
				vaciarTabla();
				try
				{
					llenarTabla(iApi.filtrarDispositivos(textBuscar.getText(), iApi.getDispositivosDTO()));
				}
				catch(ConnectionFailureException e1)
				{
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
			}
	}
	
	
	private void actualizarEstadoBtn() throws ConnectionFailureException {
	    int filaSelec = jTable.getSelectedRow();
	    if (filaSelec != -1) {
	        String nombreDispositivo = (String) jTable.getModel().getValueAt(filaSelec, 0);
	        DispositivoDTO dispositivoDTO = iApi.recuperarDispositivoDTO(nombreDispositivo);
	        
	        btnActivarDispositivo.setEnabled(!dispositivoDTO.isEstado());
	        btnDesactivarDispositivo.setEnabled(dispositivoDTO.isEstado());
	        btnModificarDispositivo.setEnabled(true);
	        btnEliminar.setEnabled(true);
	    } else {
	        btnActivarDispositivo.setEnabled(false);
	        btnDesactivarDispositivo.setEnabled(false);
	        btnModificarDispositivo.setEnabled(false);
	        btnEliminar.setEnabled(false);
	
	    }
	}
	
	
	
	
	private void refreshTable() throws ConnectionFailureException {
		vaciarTabla();
		llenarTabla(iApi.getDispositivosDTO());
	}
	
	private void vaciarTabla() {
		modelo.setRowCount(0);
		modelo.fireTableDataChanged();
	}
	
	private void llenarTabla(List<DispositivoDTO> listaDispositivos) throws ConnectionFailureException {
		for (DispositivoDTO u : listaDispositivos) {
			String estado = "DESACTIVADO";
			if(u.isEstado()) {
				estado = "ACTIVO";
			}
			modelo.addRow(new Object[] { u.getNombre(), u.getModelo(), u.getMarca(), u.getDescripcion(), estado,u.getListaParametros()});
		}
		jTable.setModel(modelo);
	}
}