package ar.edu.unrn.seminario.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import ar.edu.unrn.seminario.dto.DispositivoDTO;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.InvalidStateChangeException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class VentanaModificarDispositivo extends JFrame implements ActionListener {

    private JPanel contentPane;
    private JTextField textBoxNombre;
    private JTextField textBoxMarca;
    private JTextField textBoxModelo;
    private JTextArea textPaneDescripcion;
    private JRadioButton rdbtnActivado;
    private JRadioButton rdbtnDesactivado;
    private JButton btnGuardar;
    private JButton btnCancelar;
    private JComboBox <String> comboBoxConfiguraciones;
    private ButtonGroup bg = new ButtonGroup();
    private IApi iApi;
    private DispositivoDTO dto;
    private JScrollPane jScrollPaneTextArea;

    
    /**
     * @wbp.parser.constructor
     */
    public VentanaModificarDispositivo(IApi iApi, DispositivoDTO dispositivoDTO) throws ConnectionFailureException {
    	this.iApi=iApi;
    	dto=dispositivoDTO;
    	setTitle("Modificacion de dispositivo");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 438, 427);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        
        setLocationRelativeTo(this);
        setContentPane(contentPane);
        contentPane.setLayout(null);
        

        textBoxNombre = new JTextField();
        textBoxNombre.setEditable(false);
        textBoxNombre.setBounds(22, 46, 126, 20);
        textBoxNombre.setText(dispositivoDTO.getNombre());
        contentPane.add(textBoxNombre);
        textBoxNombre.setColumns(10);
        
        JLabel lblMarca = new JLabel("Marca");
        lblMarca.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblMarca.setBounds(22, 79, 68, 14);
        contentPane.add(lblMarca);

        textBoxMarca = new JTextField();
        textBoxMarca.setColumns(10);
        textBoxMarca.setBounds(22, 103, 126, 20);
        textBoxMarca.setText(dispositivoDTO.getMarca());
        contentPane.add(textBoxMarca);
        
        JLabel lblModelo = new JLabel("Modelo");
        lblModelo.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblModelo.setBounds(22, 134, 68, 14);
        contentPane.add(lblModelo);

        textBoxModelo = new JTextField();
        textBoxModelo.setColumns(10);
        textBoxModelo.setBounds(22, 159, 126, 20);
        textBoxModelo.setText(dispositivoDTO.getModelo());
        contentPane.add(textBoxModelo);
    
        
        textPaneDescripcion = new JTextArea();
        textPaneDescripcion.setText(dispositivoDTO.getDescripcion());
        
        textPaneDescripcion.setLineWrap(true);
        textPaneDescripcion.setWrapStyleWord(true);
        
        jScrollPaneTextArea = new JScrollPane(textPaneDescripcion);
		jScrollPaneTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPaneTextArea.setBounds(22, 225, 192, 118);
		
		contentPane.add(jScrollPaneTextArea);
        
        JLabel lblNewLabel_1 = new JLabel("Nombre");
        lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblNewLabel_1.setBounds(22, 21, 68, 14);
        contentPane.add(lblNewLabel_1);

        JLabel lblDescripcion = new JLabel("Descripcion");
        lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblDescripcion.setBounds(22, 193, 109, 23);
        contentPane.add(lblDescripcion);
        
        JLabel lblConfiguracion = new JLabel("Configuraciones");
        lblConfiguracion.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblConfiguracion.setBounds(224, 133, 163, 23);
        contentPane.add(lblConfiguracion);

        JLabel lblEstado = new JLabel("Estado");
        lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblEstado.setBounds(242, 21, 89, 23);
        contentPane.add(lblEstado);
        
        btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(this) ;
        btnCancelar.setBounds(298, 354, 89, 23);
        contentPane.add(btnCancelar);        
        
        btnGuardar = new JButton("Guardar");
        btnGuardar.addActionListener(this);
        btnGuardar.setBounds(199, 354, 89, 23);
        contentPane.add(btnGuardar);  
        
        comboBoxConfiguraciones = new JComboBox<String>();
        comboBoxConfiguraciones.setBounds(224, 160, 163, 22);
        contentPane.add(comboBoxConfiguraciones);
        
        rdbtnActivado = new JRadioButton("Activado");
        rdbtnActivado.setFont(new Font("Tahoma", Font.PLAIN, 16));
        rdbtnActivado.setBounds(238, 44, 109, 23);
        contentPane.add(rdbtnActivado);
        rdbtnActivado.addActionListener(this);

        rdbtnDesactivado = new JRadioButton("Desactivado");
        rdbtnDesactivado.setFont(new Font("Tahoma", Font.PLAIN, 16));
        rdbtnDesactivado.setBounds(238, 70, 149, 23);
        contentPane.add(rdbtnDesactivado);
        rdbtnDesactivado.addActionListener(this);
        
        bg.add(rdbtnActivado);
        bg.add(rdbtnDesactivado);

        if (dispositivoDTO.isEstado()) {
            rdbtnActivado.setSelected(true);
        } else {
            rdbtnDesactivado.setSelected(true); 
        }  
        
        llenarComboBox();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == btnCancelar) {
			try
			{
				VentanaDispositivo ventanaDispositivo = new VentanaDispositivo(iApi);
				ventanaDispositivo.setVisible(true);
				ventanaDispositivo.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		else if (e.getSource() == btnGuardar)
		{
			try {
	    		if(rdbtnActivado.isSelected()){
	    			isComplet(textBoxNombre.getText(), "nombre");
	    			isComplet(textBoxModelo.getText(), "modelo");
	    			isComplet(textBoxMarca.getText(), "marca");
	    		}
	    	
	    		String nuevaDescripcion = textPaneDescripcion.getText();
	    		String nuevoModelo = textBoxModelo.getText();
	    		String nuevaMarca = textBoxMarca.getText();
	    		boolean nuevoEstado = rdbtnActivado.isSelected(); 
	    		String configuracion = iApi.buscarConfiguracionDTO((String) comboBoxConfiguraciones.getSelectedItem()).getNombre();
	
	    		iApi.modificarDispositivo(dto.getNombre(), nuevaDescripcion, nuevoModelo, nuevaMarca, nuevoEstado,configuracion);
	    		
	    		JOptionPane.showMessageDialog(null, "El dispositivo se modifico correctamente");
	    		VentanaDispositivo ventana = new VentanaDispositivo(iApi);
	    		ventana.setLocationRelativeTo(null);
	    		ventana.setVisible(true);
	    		this.dispose();
	    		}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
			catch(InvalidStateChangeException g)
			{
				JOptionPane.showMessageDialog(null, g.getMessage());
			}
			catch(DataEmptyException dee)
			{
				String mensaje = dee.getMessage();
				JOptionPane.showMessageDialog(null, mensaje, "ERROR", JOptionPane.ERROR_MESSAGE);
			} 
  
		}
		}
	
    
    
	private void isComplet(String campo, String nombre) throws DataEmptyException{ 
		if(campo.trim().isEmpty()) {
			throw new DataEmptyException("falta el campo " + nombre);
		}
	}
	
	
	private void llenarComboBox() throws ConnectionFailureException {
	    comboBoxConfiguraciones.removeAllItems();
	    
	    comboBoxConfiguraciones.addItem("");
	    List <ConfiguracionDTO> configuracionDTO = iApi.getConfiguracionDTO();
        for (ConfiguracionDTO dto : configuracionDTO) {
            comboBoxConfiguraciones.addItem(dto.getNombre());
        }
	}
}