package ar.edu.unrn.seminario.gui;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.MemoryApi;
import ar.edu.unrn.seminario.api.PersistenceApi;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class VPrincipalGrupo2 extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnConfiguracion;
	private JButton btnParametro;
	private JButton btnUnidad;
	private JButton btnDispositivo;
	private IApi iApi;

	public VPrincipalGrupo2(IApi memo) {
		this.iApi=memo;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnConfiguracion = new JButton("CONFIGURACION");
		btnConfiguracion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnConfiguracion.addActionListener(this);
		btnConfiguracion.setBounds(110, 90, 210, 31);
		contentPane.add(btnConfiguracion);
		
		btnParametro = new JButton("PARAMETRO");
		btnParametro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnParametro.setBounds(110, 146, 210, 31);
		contentPane.add(btnParametro);
		btnParametro.addActionListener(this);
		
		btnUnidad = new JButton("UNIDAD DE MEDIDA");
		btnUnidad.addActionListener(this);
		btnUnidad.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnUnidad.setBounds(110, 201, 210, 31);
		contentPane.add(btnUnidad);
		
		btnDispositivo = new JButton("DISPOSITIVO");
		btnDispositivo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnDispositivo.setBounds(110, 36, 210, 31);
		btnDispositivo.addActionListener(this);
		contentPane.add(btnDispositivo);		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnConfiguracion)
		{
			try
			{
				VentanaConfiguracion ventanaConfiguracion = new VentanaConfiguracion(this.iApi);
				ventanaConfiguracion.setVisible(true);
				ventanaConfiguracion.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		}
		
		else if(e.getSource() == btnParametro)
		{
			try
			{
				VentanaParametros ventParametro = new VentanaParametros(this.iApi);
				ventParametro.setVisible(true);
				ventParametro.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		}
		else if(e.getSource() == btnUnidad)
		{
			try
			{
				VentanaUnidadMedida ventMedida= new VentanaUnidadMedida(this.iApi);
				ventMedida.setVisible(true);
				ventMedida.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}

		}
		else if(e.getSource() == btnDispositivo)
		{
			try
			{
				VentanaDispositivo ventanaDispositivo = new VentanaDispositivo(this.iApi);
				ventanaDispositivo.setVisible(true);
				ventanaDispositivo.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		}
	}
}