package ar.edu.unrn.seminario.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.MemoryApi;
import ar.edu.unrn.seminario.dto.*;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTable;

public class VentanaAgregarParametro extends JFrame implements ActionListener, ListSelectionListener{

	private JPanel contentPane;
	private JTextField textNombre;
	private JTextArea textDescripcion;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private IApi iApi;
	private JTable jTable;
	private DefaultTableModel modelo;
	private JScrollPane jScrollPane;
	private JScrollPane jScrollPaneTextArea;
	private String[] titulos = {"Nombre", "Tipo", "Simoblo"};
	private JComboBox cBoxUnidadPrincipal;
	private DefaultComboBoxModel modeloCBox;
	
	public VentanaAgregarParametro(IApi iApi) throws ConnectionFailureException
	{	
		this.iApi = iApi;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 692, 359);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNombre.setBounds(10, 16, 79, 29);
		contentPane.add(lblNombre);
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDescripcion.setBounds(333, 10, 110, 29);
		contentPane.add(lblDescripcion);
		
		JLabel lblUnidadDeMedida = new JLabel("Unidad de Medida");
		lblUnidadDeMedida.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblUnidadDeMedida.setBounds(10, 117, 169, 29);
		contentPane.add(lblUnidadDeMedida);
		
		textNombre = new JTextField();
		textNombre.setColumns(10);
		textNombre.setBounds(99, 16, 159, 29);
		contentPane.add(textNombre);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAceptar.setBounds(234, 287, 98, 29);
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCancelar.setBounds(378, 287, 98, 29);
		contentPane.add(btnCancelar);
		
		textDescripcion = new JTextArea();
	
        textDescripcion.setLineWrap(true);
        textDescripcion.setWrapStyleWord(true);
		
		modelo = new DefaultTableModel(titulos, 0);
		
		jTable = new JTable(modelo);
		contentPane.add(jTable);
		
		jScrollPaneTextArea = new JScrollPane(textDescripcion);
		jScrollPaneTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPaneTextArea.setBounds(453, 16, 159, 87);
		
		contentPane.add(jScrollPaneTextArea);
		
		jScrollPane = new JScrollPane(jTable);
		contentPane.add(jScrollPane);
		jScrollPane.setBounds(234, 128, 413, 129);
		
		JLabel lblUnidadPrincipal = new JLabel("Unidad Principal");
		lblUnidadPrincipal.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblUnidadPrincipal.setBounds(10, 180, 169, 29);
		contentPane.add(lblUnidadPrincipal);
		
		modeloCBox = new DefaultComboBoxModel<>();
		
		cBoxUnidadPrincipal = new JComboBox();
		cBoxUnidadPrincipal.setModel(modeloCBox);
		cBoxUnidadPrincipal.setBounds(10, 219, 150, 21);
		contentPane.add(cBoxUnidadPrincipal);
		
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);
		
		jTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		jTable.getSelectionModel().addListSelectionListener(this);
		
		llenarTabla(iApi.getUnidadMedidaDTO());
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == btnAceptar)
		{
			
			//el bucle for obtiene las celdas seleccionadas de la tabla y las guarda en una lista de tipo String
			int[] filasSeleccionadas = jTable.getSelectedRows();
	        List<String> unidadesSeleccionadas = new ArrayList<>();
			for (int fila : filasSeleccionadas)
			{
	            String nombre = (String) jTable.getValueAt(fila, 0);
	            unidadesSeleccionadas.add(nombre);
	        }
			try
			{
				iApi.altaParametro(textNombre.getText(), unidadesSeleccionadas, textDescripcion.getText(), (String) cBoxUnidadPrincipal.getSelectedItem());
				JOptionPane.showMessageDialog(null, "El parametro se creo correctamente");
				
				textNombre.setText("");
				textDescripcion.setText("");
			}
			catch(DataEmptyException | RepeatedNameException l)
			{
				JOptionPane.showMessageDialog(null, "ATENCION: " + l.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		
		else if(e.getSource() == btnCancelar)
		{
			try
			{
				VentanaParametros ventanaNueva = new VentanaParametros(iApi);
				ventanaNueva.setVisible(true);
				ventanaNueva.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
	}
	
	private void llenarTabla(List<UnidadMedidaDTO> listaUnidadMedida)
	{
		for (UnidadMedidaDTO unidadMedidaDTO : listaUnidadMedida)
		{
			Object[] fila = new Object[4];
			fila[0] = unidadMedidaDTO.getNombre();
			fila[1] = unidadMedidaDTO.getTipoDato();
			fila[2] = unidadMedidaDTO.getSimbolo();
			
			modelo.addRow(fila);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		modeloCBox.removeAllElements();
		int[] filasSeleccionadas= jTable.getSelectedRows();
		
		for (int fila : filasSeleccionadas)
		{
			modeloCBox.addElement(jTable.getValueAt(fila, 0));
		}
	}
}
