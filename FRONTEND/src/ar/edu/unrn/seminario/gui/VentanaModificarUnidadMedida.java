package ar.edu.unrn.seminario.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.UnidadMedidaDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;

import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import java.util.Set;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class VentanaModificarUnidadMedida extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JComboBox<String> comboBoxTiposDato;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JLabel lblNombre;
	private JLabel lblTipoDato;
	private JLabel lblSimbolo;
	private JTextField textSimbolo;
	private String unidadSeleccionada;
	private IApi iApi;

	public VentanaModificarUnidadMedida(IApi iApi, String unidadMedidaModificar) throws ConnectionFailureException {
		this.iApi = iApi;
		setTitle("Modificar Unidad");
		setBounds(10, -135, 291, 315);
		unidadSeleccionada =  unidadMedidaModificar;
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.LIGHT_GRAY);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBoxTiposDato = new JComboBox();
		comboBoxTiposDato.setBounds(66, 109, 159, 28);
		contentPane.add(comboBoxTiposDato);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		for(String tipoDato : iApi.listarTiposDato()) {
			comboBoxTiposDato.addItem(tipoDato);
		}
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Dialog", Font.BOLD, 12));
		btnAceptar.setBounds(42, 226, 98, 29);
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this);
		btnCancelar.setFont(new Font("Dialog", Font.BOLD, 12));
		btnCancelar.setBounds(152, 226, 98, 29);
		contentPane.add(btnCancelar);
		
		lblNombre = new JLabel(unidadMedidaModificar);
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setFont(new Font("Dialog", Font.BOLD, 16));
		lblNombre.setBounds(23, 36, 244, 37);
		contentPane.add(lblNombre);
		
		lblTipoDato = new JLabel("Tipo de Dato");
		lblTipoDato.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblTipoDato.setBounds(72, 85, 147, 29);
		contentPane.add(lblTipoDato);
		
		lblSimbolo = new JLabel("Símbolo");
		lblSimbolo.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblSimbolo.setBounds(76, 149, 117, 29);
		contentPane.add(lblSimbolo);
		
		textSimbolo = new JTextField();
		textSimbolo.setColumns(10);
		textSimbolo.setBounds(66, 172, 159, 29);
		contentPane.add(textSimbolo);
		
		cargarDatos();
	}
	
	private void cargarDatos() throws ConnectionFailureException
	{
		comboBoxTiposDato.setSelectedItem(iApi.buscarUnidadMedidaDTO(unidadSeleccionada).getTipoDato());
		textSimbolo.setText(iApi.buscarUnidadMedidaDTO(unidadSeleccionada).getSimbolo());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnAceptar) {
			try {
				iApi.modificarUnidadMedida(unidadSeleccionada, (String) comboBoxTiposDato.getSelectedItem(),textSimbolo.getText());
				JOptionPane.showMessageDialog(null, "La unidad de medida se " + unidadSeleccionada + " ha modificado existosamente");
				VentanaUnidadMedida vtnUnidadMedida = new VentanaUnidadMedida(iApi);
				vtnUnidadMedida.setVisible(true);
				vtnUnidadMedida.setLocationRelativeTo(null);
				dispose();
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
			catch(DataEmptyException e2)
			{
				JOptionPane.showMessageDialog(null, e2.getMessage());
			}
		}
		else if(e.getSource() == btnCancelar) {
			try
			{
				VentanaUnidadMedida vtnUnidadMedida = new VentanaUnidadMedida(iApi);
				vtnUnidadMedida.setVisible(true);
				vtnUnidadMedida.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
	}
}