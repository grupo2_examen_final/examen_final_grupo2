package ar.edu.unrn.seminario.gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.api.MemoryApi;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.RepeatedNameException;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.SpringLayout;
import javax.swing.JComboBox;

public class VentanaAgregarUnidadMedida extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JComboBox comboBoxTiposDato;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JLabel lbNombre;
	private JLabel lbTipoDato;
	private JLabel lbSimbolo;
	private JTextField textNombre;
	private JTextField textSimbolo;
	private IApi iApi;

	public VentanaAgregarUnidadMedida(IApi iApi) {
		this.iApi = iApi;
		setTitle("Agregar Unidad");
		setBounds(100, 100, 280, 300);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.LIGHT_GRAY);
		setContentPane(contentPane);

		comboBoxTiposDato = new JComboBox();
		comboBoxTiposDato.setBounds(37, 107, 196, 28);
		contentPane.add(comboBoxTiposDato);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		for(String tipoDato : iApi.listarTiposDato()) {
			comboBoxTiposDato.addItem(tipoDato);
		}
		
		textNombre = new JTextField();
		textNombre.setBounds(37, 49, 196, 29);
		textNombre.setColumns(10);

		
		textSimbolo = new JTextField();
		textSimbolo.setBounds(37, 170, 196, 29);
		textSimbolo.setColumns(10);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(25, 217, 100, 29);
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Dialog", Font.BOLD, 12));

		lbNombre = new JLabel("Nombre");
		lbNombre.setBounds(37, 30, 55, 17);
		lbNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lbTipoDato = new JLabel("Tipo de Dato");
		lbTipoDato.setBounds(36, 90, 89, 17);
		lbTipoDato.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lbSimbolo = new JLabel("Símbolo");
		lbSimbolo.setBounds(37, 146, 55, 29);
		lbSimbolo.setFont(new Font("Tahoma", Font.PLAIN, 14));

		btnCancelar = new JButton("Salir");
		btnCancelar.addActionListener(this);
		
		btnCancelar.setBounds(147, 217, 104, 29);
		btnCancelar.setFont(new Font("Dialog", Font.BOLD, 12));
		contentPane.setLayout(null);
		contentPane.add(lbNombre);
		contentPane.add(textNombre);
		contentPane.add(lbTipoDato);
		contentPane.add(lbSimbolo);
		contentPane.add(textSimbolo);
		contentPane.add(btnAceptar);
		contentPane.add(btnCancelar);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == btnAceptar) {
			try {
				String nombre = textNombre.getText();
				String tipoDato = (String) comboBoxTiposDato.getSelectedItem();
				String simbolo = textSimbolo.getText();
				iApi.altaUnidadMedida(nombre, tipoDato, simbolo);
				JOptionPane.showMessageDialog(null, "La unidad de medida a sido agregada exitosamente");
				textNombre.setText("");
				comboBoxTiposDato.setSelectedIndex(0);
				textSimbolo.setText("");
			} catch (RepeatedNameException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			} 
			catch (ConnectionFailureException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}catch (DataEmptyException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
		}
		else if(event.getSource() == btnCancelar) {
			try
			{
				VentanaUnidadMedida vtnUnidadMedida = new VentanaUnidadMedida(iApi);
				vtnUnidadMedida.setVisible(true);
				vtnUnidadMedida.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
	}
}
