package ar.edu.unrn.seminario.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.ParametroDTO;
import ar.edu.unrn.seminario.exception.*;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class VentanaAgregarConfiguracion extends JFrame implements  ActionListener  {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JLabel lblDescripcion;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JTextArea textAreaDescripcion;
	private String[] titulos = {"Configuracion", "Descripcion"};
	private IApi iApi;
	private JLabel lblAgregarParametro;
    private DefaultTableModel modelo;
    private JTable jTable ;
    private JScrollPane scrollPane;
    private JScrollPane jScrollPaneTextArea;

	 

	public VentanaAgregarConfiguracion(IApi api) throws ConnectionFailureException {
		this.iApi=api;
		setTitle("Crear Configuracion");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 391);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("NOMBRE");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombre.setBounds(10, 29, 92, 14);
		contentPane.add(lblNombre);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(212, 29, 188, 20);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		lblDescripcion = new JLabel("DESCRIPCION");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDescripcion.setBounds(10, 74, 122, 20);
		contentPane.add(lblDescripcion);
	
		
		btnAceptar = new JButton("ACEPTAR");
		btnAceptar.setBounds(212, 318, 95, 23);
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("ATRAS");
		btnCancelar.setBounds(311, 318, 95, 23);
		contentPane.add(btnCancelar);
		modelo = new DefaultTableModel(titulos, 0);
        jTable = new JTable(modelo);
        scrollPane = new JScrollPane(jTable);
        scrollPane.setEnabled(false);
        scrollPane.setBounds(10, 208, 390, 100);
        contentPane.add(scrollPane);
		lblAgregarParametro = new JLabel("AGREGAR PARAMETRO");
		lblAgregarParametro.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAgregarParametro.setBounds(10, 184, 204, 14);
		contentPane.add(lblAgregarParametro);
		
		textAreaDescripcion = new JTextArea();

        textAreaDescripcion.setLineWrap(true);
        textAreaDescripcion.setWrapStyleWord(true);
        
        jScrollPaneTextArea = new JScrollPane(textAreaDescripcion);
		jScrollPaneTextArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jScrollPaneTextArea.setBounds(212, 72, 188, 100);
		
		contentPane.add(jScrollPaneTextArea);
		btnCancelar.addActionListener(this);
		btnAceptar.addActionListener(this);

		inicializar();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	
		if(e.getSource() == btnCancelar)
		{
			try
			{
				VentanaConfiguracion ventanaAnterior = new VentanaConfiguracion(iApi);
				ventanaAnterior.setVisible(true);
				ventanaAnterior.setLocationRelativeTo(null);
				this.dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		
		else if(e.getSource() == btnAceptar) {
			
			try {
			int[] filasSeleccionadas = jTable.getSelectedRows();
	        List<String> parametrosSeleccionados = new ArrayList<>();
			for (int fila : filasSeleccionadas) {
	            String nombre = (String) jTable.getValueAt(fila, 0);
	            parametrosSeleccionados.add(nombre);
	        }
			iApi.altaConfiguracion(this.textFieldNombre.getText(),this.textAreaDescripcion.getText(),parametrosSeleccionados);
			JOptionPane.showMessageDialog(null, "Configuracion creada correctamente");
			textFieldNombre.setText("");
			textAreaDescripcion.setText("");
			}
			catch(DataEmptyException l)
			{
			    JOptionPane.showMessageDialog(null, "ATENCION: " + l.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			catch (RepeatedNameException r)
			{
				JOptionPane.showMessageDialog(null, "ATENCION: " + r.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
	
	}
	
	}
	
	private void inicializar() throws ConnectionFailureException {
		List <ParametroDTO> parametros = iApi.getParametroDTO();
		for (ParametroDTO paramDTO : parametros)
		{
			Object[] fila = new Object[5];
			fila[0] = paramDTO.getNombre();
			fila[1] = paramDTO.getDescripcion();
			fila[2] = paramDTO.getUnidadPrincipal();
			
			modelo.addRow(fila);
		}
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
}