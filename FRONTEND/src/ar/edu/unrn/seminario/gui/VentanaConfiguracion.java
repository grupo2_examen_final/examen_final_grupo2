package ar.edu.unrn.seminario.gui;

import java.awt.BorderLayout;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import ar.edu.unrn.seminario.api.IApi;
import ar.edu.unrn.seminario.dto.ConfiguracionDTO;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.ReferenceDataException;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.awt.event.ActionEvent;

public class VentanaConfiguracion extends JFrame  implements ActionListener, ListSelectionListener{

	private JPanel contentPane;
	private JTextField textFieldBuscar;
	private JTable table;
	private String[] titulos = {"Configuracion", "Descripcion","Parametros"};
	private DefaultTableModel modelo;
	private IApi iApi;
	private JButton btnAgregarConfiguracion;
	private JButton btnEliminar;
	private JButton btnModificar;
	private JButton btnBuscarConfig;
	private JButton btnRefresh;
	private JButton btnSalir;
	private JPanel panelBotonesInferior;


	public VentanaConfiguracion(IApi api) throws ConnectionFailureException {
		setTitle("Configuraciones");
		this.iApi = api;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 568, 304);
		modelo = new DefaultTableModel(titulos, 0);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panelBotonesSuperior = new JPanel();
		contentPane.add(panelBotonesSuperior);
		
		btnSalir = new JButton("Salir");
		panelBotonesSuperior.add(btnSalir);
		
		btnBuscarConfig = new JButton("Buscar Configuracion");
		panelBotonesSuperior.add(btnBuscarConfig);
		textFieldBuscar = new JTextField();
		panelBotonesSuperior.add(textFieldBuscar);
		textFieldBuscar.setText("");
		textFieldBuscar.setBounds(213, 12, 156, 20);
		textFieldBuscar.setColumns(10);
		
		btnRefresh = new JButton("Refrescar");
		panelBotonesSuperior.add(btnRefresh);
	
		btnBuscarConfig.addActionListener(this);
		btnRefresh.addActionListener(this);
		
		btnSalir.addActionListener(this);
		
		contentPane.add(panelBotonesSuperior, BorderLayout.NORTH);
		
		table = new JTable();
		table.setEnabled(true);
		contentPane.add(table, BorderLayout.CENTER);
		table.setModel(modelo);
		table.setRowSelectionAllowed(true);
		table.getSelectionModel().addListSelectionListener(this);
       

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setToolTipText("Configuraciones");
		scrollPane.setEnabled(false);
		getContentPane().add(scrollPane);
		
		panelBotonesInferior = new JPanel();
		contentPane.add(panelBotonesInferior, BorderLayout.SOUTH);
		
		btnAgregarConfiguracion = new JButton("Agregar");
		panelBotonesInferior.add(btnAgregarConfiguracion);
		
		btnModificar = new JButton("Modificar");
		panelBotonesInferior.add(btnModificar);
		btnModificar.addActionListener(this);
		
		btnEliminar = new JButton("Eliminar");
		panelBotonesInferior.add(btnEliminar);
		btnEliminar.addActionListener(this);
		btnAgregarConfiguracion.addActionListener(this);
		
		btnEliminar.setEnabled(false);
		btnModificar.setEnabled(false);
		
		table.getSelectionModel().addListSelectionListener(this);

		inicializarTabla();
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == btnAgregarConfiguracion)
		{
			try {
				if (!iApi.getParametroDTO().isEmpty()) {
					VentanaAgregarConfiguracion ventanaConfiguracion = new VentanaAgregarConfiguracion(iApi);
					ventanaConfiguracion.setVisible(true);
					ventanaConfiguracion.setLocationRelativeTo(null);
					this.dispose();
				}else {
					JOptionPane.showMessageDialog(null, "No hay parametros disponibles. Primero inicializa algun parametro");


				}
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		
		else if(e.getSource() == btnModificar)
		{	int filaSeleccionada = table.getSelectedRow();
		    try {
				if (filaSeleccionada != -1) {
			        String nombre = (String) modelo.getValueAt(filaSeleccionada, 0);
			        VentanaModificarConfiguracion ventanaModificar = new VentanaModificarConfiguracion(iApi,nombre,this);
					ventanaModificar.setVisible(true);
					ventanaModificar.setLocationRelativeTo(null);
					this.dispose();
			    }
		    }
		    catch (ConnectionFailureException e1)
		    {
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			} 
			

		}
		
		else if(e.getSource() == btnBuscarConfig)
		{
			limpiarTabla();
			try
			{
				llenarTabla(iApi.filtrarConfiguracionDTO(textFieldBuscar.getText(), iApi.getConfiguracionDTO()));
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}
		else if(e.getSource() == btnRefresh)
		{
			limpiarTabla();
			try
			{
				llenarTabla(iApi.getConfiguracionDTO());
			}
			catch (ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
			
			
		}
		else if(e.getSource() == btnSalir)
		{
			VPrincipalGrupo2 ventanaGrupo= new VPrincipalGrupo2 (iApi);
			ventanaGrupo.setVisible(true);
			ventanaGrupo.setLocationRelativeTo(null);
			this.dispose();
		}
		else if (e.getSource() == btnEliminar)
		{
			int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar la configuracion?", "Confirmación", JOptionPane.YES_NO_OPTION);
		    if (respuesta == JOptionPane.YES_OPTION)
		    {
			    int filaSeleccionada = table.getSelectedRow();
		        try
		        {
			        iApi.eliminarConfiguracion((String) modelo.getValueAt(filaSeleccionada, 0));
		            limpiarTabla();
		            llenarTabla(iApi.getConfiguracionDTO());
		            JOptionPane.showMessageDialog(null, "La configuracion se elimino correctamente");
		        }
		        catch (ConnectionFailureException e1)
		        {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
		        catch (ReferenceDataException e3)
		        {
		        	JOptionPane.showMessageDialog(null, e3.getMessage());
		        }
		    }
		    else 
			{
				JOptionPane.showMessageDialog(null, "Se ha cancelado la eliminacion");
			}
		}
	}


	private void limpiarTabla()
	{
		modelo.setRowCount(0);
		modelo.fireTableDataChanged();
	}
	private void llenarTabla(List<ConfiguracionDTO> lista) throws ConnectionFailureException
	{	
		for (ConfiguracionDTO configuracionDTO : lista)
		{
			Object[] fila = new Object[5];
			fila[0] = configuracionDTO.getNombre();
			fila[1] = configuracionDTO.getDescripcion();
			fila[2] = configuracionDTO.getListaParametros();
			modelo.addRow(fila);
		}
	}
	
	private void inicializarTabla() throws ConnectionFailureException
	{
		limpiarTabla();
		llenarTabla(iApi.getConfiguracionDTO());
	}

	@Override
	public void valueChanged(ListSelectionEvent e)
	{
		if(table.getSelectedRowCount() == 1)
		{
			btnEliminar.setEnabled(true);
			btnModificar.setEnabled(true);
		}
		
		else if(table.getSelectedRowCount()>1)
		{
			btnEliminar.setEnabled(false);
			btnModificar.setEnabled(false);
		}
	}
}