package ar.edu.unrn.seminario.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import ar.edu.unrn.seminario.api.*;
import ar.edu.unrn.seminario.dto.*;
import ar.edu.unrn.seminario.exception.ConnectionFailureException;
import ar.edu.unrn.seminario.exception.ReferenceDataException;

import javax.swing.JToolBar;

public class VentanaUnidadMedida extends JFrame implements ActionListener, ListSelectionListener{

	private JPanel contentPane;
	private String[] titulos = {"Unidad de medida", "Símbolo", "Tipo de dato"};
	private DefaultTableModel modelo;
	private JMenuBar menuBar;
	private JToolBar toolBar;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnBuscar;
	private JButton btnAgregar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton btnSalir;
	private JTextField textBuscar;
	private IApi iApi;
	private List<UnidadMedidaDTO> listaUnidadesMedida = new ArrayList<>();

	
	
	public VentanaUnidadMedida(IApi iApi) throws ConnectionFailureException	{
		this.iApi = iApi;
		
		setTitle("Unidad de Medida");
		setBounds(100, 100, 559, 376);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.lightGray);
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		modelo = new DefaultTableModel(titulos, 0);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		toolBar = new JToolBar();
		contentPane.add(toolBar, BorderLayout.SOUTH);
		toolBar.setBackground(Color.lightGray);
		
		table = new JTable();
		contentPane.add(table, BorderLayout.CENTER);
		table.setModel(modelo);
		table.getSelectionModel().addListSelectionListener(this);
		
		scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane);
		
		btnAgregar = new JButton("Agregar");
		toolBar.add(btnAgregar);
		btnAgregar.addActionListener(this);
		
		btnModificar = new JButton("Modificar");
		toolBar.add(btnModificar);
		btnModificar.addActionListener(this);
		btnModificar.setEnabled(false);
		
		btnEliminar = new JButton("Eliminar");
		toolBar.add(btnEliminar);
		btnEliminar.addActionListener(this);
		btnEliminar.setEnabled(false);
		
		btnSalir = new JButton("Salir");
		menuBar.add(btnSalir);
		
		btnSalir.addActionListener(this);
		
		textBuscar = new JTextField();
		menuBar.add(textBuscar);
		textBuscar.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		menuBar.add(btnBuscar);
		btnBuscar.addActionListener(this);
		
		inicializarTabla();

		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
      
	}
	
	@Override
    public void valueChanged(ListSelectionEvent e)
	{
		if (!e.getValueIsAdjusting()) 
		{
			 if (e.getSource() == table.getSelectionModel()) 
			 {
				if (table.getSelectedRowCount() == 1) 
				{
					btnEliminar.setEnabled(true);
					btnModificar.setEnabled(true);								
				}
				else 
				{
					btnModificar.setEnabled(false);
					btnEliminar.setEnabled(false);
				} 			
			}
		}
	}
	@Override
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == btnAgregar)
		{
			VentanaAgregarUnidadMedida vtnAgregar = new VentanaAgregarUnidadMedida(iApi);
			vtnAgregar.setVisible(true);
			vtnAgregar.setLocationRelativeTo(null);
			dispose();
		}
		
		else if(e.getSource() == btnModificar){
			try
			{
				VentanaModificarUnidadMedida vtnModificar = new VentanaModificarUnidadMedida(iApi, (String) table.getValueAt(table.getSelectedRow(), 0));
				vtnModificar.setVisible(true);
				vtnModificar.setLocationRelativeTo(null);
				dispose();
			}
			catch(ConnectionFailureException e1)
			{
				JOptionPane.showMessageDialog(null, e1.getMessage());
				VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
				nuevaVentana.setLocationRelativeTo(null);
				nuevaVentana.setVisible(true);
				this.dispose();
			}
		}	
		
		else if(e.getSource() == btnBuscar){
			limpiarTabla();
			
			if(textBuscar.getText().equals(null) || textBuscar.getText().equals(""))
			{
				llenarTabla(listaUnidadesMedida);
			}
			else 
			{
				llenarTabla(iApi.filtrarUnidadMedida(textBuscar.getText(), listaUnidadesMedida));					
			}
		}
		
		else if(e.getSource() == btnSalir)
		{
			VPrincipalGrupo2 ventanaGrupo= new VPrincipalGrupo2 (iApi);
			ventanaGrupo.setVisible(true);
			ventanaGrupo.setLocationRelativeTo(null);
			this.dispose();
		}
		
		else if(e.getSource() == btnEliminar)
		{
			int respuesta = JOptionPane.showConfirmDialog(null, "¿Está seguro de eliminar la unidad de medida?", "Confirmación", JOptionPane.YES_NO_OPTION);
			if (respuesta == JOptionPane.YES_OPTION) 
			{
				try
				{
					iApi.eliminarUnidadeMedida((String) table.getValueAt(table.getSelectedRow(), 0));
					
					limpiarTabla();
					llenarTabla(iApi.getUnidadMedidaDTO());
					
					JOptionPane.showMessageDialog(null, "Se ha eliminado la unidad de medida");
				}
				catch(ConnectionFailureException j)
				{
					JOptionPane.showMessageDialog(null, j.getMessage());
					VPrincipalGrupo2 nuevaVentana = new VPrincipalGrupo2(iApi);
					nuevaVentana.setLocationRelativeTo(null);
					nuevaVentana.setVisible(true);
					this.dispose();
				}
				catch (ReferenceDataException e2)
				{
					JOptionPane.showMessageDialog(null, e2.getMessage());
				}
			} 
			else 
			{
				JOptionPane.showMessageDialog(null, "Se ha cancelado la eliminacion");
			}
		}
	}
	
	private void llenarTabla(List<UnidadMedidaDTO> list){
		for (UnidadMedidaDTO unidadMedidaDTO : list){
			Object[] fila = new Object[3];
			fila[0] = unidadMedidaDTO.getNombre();
			fila[1] = unidadMedidaDTO.getSimbolo();
			fila[2] = unidadMedidaDTO.getTipoDato();
			
			modelo.addRow(fila);
		}
	}
	
	private void limpiarTabla(){
		modelo.setRowCount(0);
		modelo.fireTableDataChanged();
	}
	
	private void inicializarTabla() throws ConnectionFailureException
	{
		listaUnidadesMedida = iApi.getUnidadMedidaDTO();
		llenarTabla(listaUnidadesMedida);
	}
}
