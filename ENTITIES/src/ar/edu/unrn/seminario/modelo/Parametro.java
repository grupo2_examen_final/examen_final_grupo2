package ar.edu.unrn.seminario.modelo;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class Parametro
{
	private String nombre;
	private Set<UnidadMedida> unidadMedida;
	private String descripcion;
	private UnidadMedida unidadPrincipal;
	
	
	public Parametro() {
		
	}
	
	public Parametro(String nombre, Set<UnidadMedida> unidadMedida, String descripcion, UnidadMedida unidadPrincipal) throws DataEmptyException
	{
		verificarVacio(nombre, unidadMedida, descripcion, unidadPrincipal);
		this.nombre = nombre.trim();
		this.unidadMedida = unidadMedida;
		this.descripcion = descripcion.trim();
		this.unidadPrincipal = unidadPrincipal;
	}
	
	private void verificarVacio(String nombre, Set<UnidadMedida> unidadMedida, String descripcion, UnidadMedida unidadPrincipal) throws DataEmptyException
	{
		if(unidadMedida == null || unidadMedida.isEmpty() )
		{
			throw new DataEmptyException("Debe seleccionar al menos una unidad de medida");
		}
		else if(nombre == null || nombre.isBlank())
		{
			throw new DataEmptyException("El campo nombre no puede estar vacio");
		}
		else if(descripcion == null || descripcion.isBlank())
		{
			throw new DataEmptyException("El parametro debe tener una descripcion");
		}
		else if(unidadPrincipal == null)
		{
			throw new DataEmptyException("Debe seleccionar una unidad principal");
		}
	}

	public String getNombre() {
		return nombre;
	}

	public Set<UnidadMedida> getUnidadMedida() {
		return unidadMedida;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public UnidadMedida getUnidadPrincipal() {
		return unidadPrincipal;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.trim();
	}

	public void setUnidadMedida(Set<UnidadMedida> unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion.trim();
	}

	public void setUnidadPrincipal(UnidadMedida unidadPrincipal) {
		this.unidadPrincipal = unidadPrincipal;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parametro other = (Parametro) obj;
		return Objects.equals(nombre.toLowerCase(), other.nombre);
	}
}
