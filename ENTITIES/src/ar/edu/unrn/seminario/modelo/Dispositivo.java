package ar.edu.unrn.seminario.modelo;


import java.util.Objects;

import ar.edu.unrn.seminario.exception.DataEmptyException;
import ar.edu.unrn.seminario.exception.InvalidStateChangeException;

public class Dispositivo{	
	private boolean estado = false;
	private String nombre;
	private String descripcion;
	private String modelo;
	private String marca;
	private Configuracion configuracion;
	
	
	public Dispositivo() {
		
	}
	
	public Dispositivo(String nombre, String descripcion, String modelo, String marca, boolean estado, Configuracion configraucion) throws DataEmptyException, InvalidStateChangeException{
		verificarVacio(nombre, marca, modelo);
		this.nombre = nombre.trim();
		this.descripcion = descripcion.trim();
		this.modelo = modelo.trim();
		this.marca = marca.trim();
		this.configuracion=configraucion;
		if(estado) {
			activar();
		}
	}

	
	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.trim();
	}

	public String getDescripcion() {
		return descripcion.trim();
	}

	public Configuracion getConfiguracion() {
		return configuracion;
	}


	public void setConfiguracion(Configuracion configuracion) {
		this.configuracion = configuracion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion.trim();
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo.trim();
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca.trim();
	}
	
	public void activar() throws InvalidStateChangeException{
		if(cumpleCondicion()) {
			this.estado = true;
			}
		else {
			throw new InvalidStateChangeException("Para poder activar el dispositivo el mismo debe tener una configuracion");
		}
		
	}

	public void desactivar() {
		if (getEstado())
			this.estado = false;
	}
	
	
	private void verificarVacio(String nombre, String marca, String modelo) throws DataEmptyException
	{
		if(nombre == null || nombre.isBlank())
		{
			throw new DataEmptyException("El campo nombre no puede estar vacio");
		}
		else if(marca == null || marca.isBlank())
		{
			throw new DataEmptyException("El dispositivo debe tener una marca");
		}
		else if(modelo == null || modelo.isBlank())
		{
			throw new DataEmptyException("El dispositivo debe tener un modelo");
		}
	}

	private boolean cumpleCondicion() {
		return (!(configuracion.getNombre() == null || (configuracion.getNombre().isBlank())));
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dispositivo other = (Dispositivo) obj;
		return Objects.equals(nombre, other.nombre);
	}
}