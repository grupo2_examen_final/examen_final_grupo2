package ar.edu.unrn.seminario.modelo;

import java.util.Set;

import ar.edu.unrn.seminario.exception.DataEmptyException;

public class UnidadMedida
{
	private String nombre;
	private String tipoDato;
	private String simbolo;
	
	public UnidadMedida() {
		
	}
	
	public UnidadMedida(String nombre, String tipoDato, String simbolo) throws DataEmptyException
	{
		verificarCampo(nombre, "El campo nombre está vacio");	
		verificarCampo(tipoDato, "El campo tipo de dato está vacio");
		verificarCampo(simbolo, "El campo símbolo está vacio");
		this.nombre = nombre.trim();
		this.tipoDato = tipoDato.trim();
		this.simbolo = simbolo.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre.trim();
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato.trim();
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo.trim();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadMedida other = (UnidadMedida) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
	private void verificarCampo(String campo, String mensaje) throws DataEmptyException
	{
		if(campo == null || campo.isBlank()) {
			throw new DataEmptyException(mensaje);
		}
	}
}
