package ar.edu.unrn.seminario.modelo;
import ar.edu.unrn.seminario.exception.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;



public class Configuracion  {
	private String nombre;
	private String descripcion;
	private Set<Parametro> listaParametros= new HashSet<>();
	
	
	public Configuracion(){
	}

	public Configuracion(String nombre, String descripcion, Set <Parametro> parametros) throws DataEmptyException{
		
			verificarCampos( nombre, descripcion, parametros);
			this.nombre=nombre.trim();
			this.descripcion=descripcion.trim();
			this.listaParametros=parametros;
	}
	

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.trim();
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion.trim();
	}
	
	public void setListaParametros(Set <Parametro> lista) {
		this.listaParametros = lista;
	}
	
	

	public Set<Parametro> getListaParametros() {
		return listaParametros;
	}

	public void agregarParametro (Parametro parametro) throws RepeatedNameException {
		for (Parametro param : this.listaParametros) {
			if (param.equals(parametro)) {
				throw new RepeatedNameException("El parametro ya esta en la Configuracion");
			}
		}
		this.listaParametros.add(parametro);
	}
	
	public List <String> parsearParametros (){
		List <String> lista= new ArrayList<>();
		for (Parametro p : listaParametros) {
			lista.add(p.getNombre());
		}
	return lista;
	}
	
	public void eliminarParametro(String Nombre) {
	    Iterator<Parametro> iterador = this.listaParametros.iterator();
	    while (iterador.hasNext()) {
	        Parametro parametro = iterador.next();
	        if (parametro.getNombre().equalsIgnoreCase(Nombre)) {
	            iterador.remove();
	        } 
	    }
	}
	
	
	public void verificarCampos(String nombre, String descripcion,Set <Parametro> lista) throws DataEmptyException {
		if (lista == null || lista.isEmpty()) {
			throw new DataEmptyException("Debe seleccionar al menos un parametro");
		}
		else if(nombre.isEmpty() || nombre.isEmpty() || nombre.isBlank()){
			throw new DataEmptyException("El nombre no puede estar vacio");
		}
		else if(descripcion.isEmpty() || descripcion.isEmpty() || descripcion.isBlank()){
			throw new DataEmptyException("La descripcion no puede estar vacia");
		}
	}


	@Override
	public String toString() {
		return "Configuracion [nombre=" + nombre + ", descripcion=" + descripcion + ", listaParametros="
			 + this.parsearParametros() + "]";
	}
	
	public void eliminarTodosLosParametros() {
		this.listaParametros.clear();
	}


	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuracion other = (Configuracion) obj;
		return Objects.equals(nombre, other.nombre);
	}
}
