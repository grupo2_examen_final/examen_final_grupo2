package ar.edu.unrn.seminario.exception;

public class ConnectionFailureException extends Exception
{
	private String mensaje;
	
	public ConnectionFailureException() {
	
	}
	
	public ConnectionFailureException(String mensaje) {
		this.mensaje=mensaje;
	}
	
	@Override
	public String getMessage() {
	    return mensaje;
	}
}
