package ar.edu.unrn.seminario.exception;

public class DataEmptyException extends Exception{
	
	private String mensaje;
	
	public DataEmptyException() {
	
	}
	
	public DataEmptyException(String mensaje) {
		this.mensaje=mensaje;
	}
	
	@Override
	public String getMessage() {
	    return mensaje;
	}
}