package ar.edu.unrn.seminario.exception;

public class RepeatedNameException  extends Exception{
	private String mensaje;


public RepeatedNameException(String mensaje) {
	this.mensaje=mensaje;
}

@Override
public String getMessage() {
    return mensaje;

}
}

