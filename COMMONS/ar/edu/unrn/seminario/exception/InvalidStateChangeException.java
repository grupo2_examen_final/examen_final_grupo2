package ar.edu.unrn.seminario.exception;

public class InvalidStateChangeException extends RuntimeException{
	public InvalidStateChangeException() {
		
	}
	public InvalidStateChangeException(String message) {
		super(message);
	}
}