package ar.edu.unrn.seminario.exception;

/**
	Se utiliza cuando ya existe un objeto o entidad con el mismo valor clave,
	por ejemplo, cuando se quiere agregar un dispositivo con el mismo nombre.
*/

public class ObjectAlreadyExistException extends RuntimeException {
	//AGREGAR OTROS ATRIBUTOS EN CASO NECESARIO
	public ObjectAlreadyExistException () {
	}
	public ObjectAlreadyExistException (String message) {
		super(message);
	}
}