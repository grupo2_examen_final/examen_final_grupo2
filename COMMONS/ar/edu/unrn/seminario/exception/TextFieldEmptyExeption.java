package ar.edu.unrn.seminario.exception;

public class TextFieldEmptyExeption extends Exception {
	private String mensaje;


	public TextFieldEmptyExeption(String mensaje) {
        this.mensaje = mensaje;
    }
    

    @Override
    public String getMessage() {
        return mensaje;
    
    }
}
