package ar.edu.unrn.seminario.exception;

public class ReferenceDataException extends Exception {
	private String mensaje;
	
	public ReferenceDataException() {
	
	}
	
	public ReferenceDataException(String mensaje) {
		this.mensaje=mensaje;
	}
	@Override
	public String getMessage() {
	    return mensaje;
	}
}